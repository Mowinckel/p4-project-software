class Input {
  private int id;
  private boolean[] prevTime = new boolean[16];
  private int frequency;
  private int waveform;
  private int channel;
  private int chord;
  private boolean[] time = new boolean[16];

  
  Input () {
    id = 0; //Consider writing a better default constructor
  }
  
  Input (int tempID) {
     id = tempID;
  }
  
  Input (int tempID, int tempFrequency, int tempWaveform, int tempChord) {
    id = tempID;
    frequency = tempFrequency;
    waveform = tempWaveform;
    chord = tempChord;
  }
  
  
  Input (int tempId, int tempFrequency, int tempWaveform, int tempChannel, int tempChord, boolean[] prevTime, boolean[] time) {
    frequency = tempFrequency;
    waveform = tempWaveform;
    channel = tempChannel;
    this.prevTime = prevTime;
    this.time = time;
    id = tempId;
    chord = tempChord;
    
  }
  
  //for noise
  Input (int tempId, int tempChannel, int tempChord, boolean[] tempTime) {
    channel = tempChannel;
    time = tempTime;
    id = tempId;
    chord = tempChord;
  }
  
  int getChord(){
    return chord;
  }
  
 void  setTimeToFalse(boolean t) {
  t = false;
  }
  
  int getID () {
    return id;
  }
  
  void setID (int newID){
    id = newID;
  }
  
  float getFrequency () {
    switch (frequency) {

    case 0: return 261.63; //C4
    case 1: return 293.66; //D4
    case 2: return 329.63; //E4
    case 3: return 392.00; //G4
    case 4: return 440.00; //A4
      
      default: return 440.00; //A4
    }
  }
  
   String printNote() {
    switch (frequency) {
      case 0: return "C4";
      case 1: return "D4";
      case 2: return "E4";
      case 3: return "G4";
      case 4: return "A4";
      
      default: return "A4";
    }
  }
  
   float getDyad() {
     switch (frequency) {
      case 0: return 329.6; //E4
      case 1: return 370.0; //F#4
      case 2: return 415.30; //G#4
      case 3: return 493.9; //B4
      case 4: return 554.4; //C#5
      
      default: return 440.00; //A4
     }
   }
  
  float getTriad() {
    switch (frequency) {
      case 0:  return 493.88; //B4
      case 1:  return 554.37; //C#5
      case 2: return 622.25; //D#5
      case 3: return 739.99; //F#5
      case 4: return 830.61; //G#5
      
      default: return 440.00; //A4
     }
  }
  
  
  Wavetable getWaveform () {
   switch (waveform) {
      case 0: return Waves.SQUARE;
      case 1: return Waves.TRIANGLE;
      case 2: return Waves.SAW;
      case 3: return Waves.QUARTERPULSE;
      default: return Waves.SQUARE;
    }
  }
  
  String printWaveform() {
    switch (waveform) {
        case 0: return "Square";
        case 1: return "Triangle";
        case 2: return "Saw";
        case 3: return "Quarterpulse";
        default: return "Square";
      }
  }
  
  
  int getChannel () {
    return channel;
  }
  
  boolean getTime (int index) {
    return time[index];
  }
  
  boolean[] getTimeArray () {
    return time;
  }
  
  void setTimeArray(boolean[] newArray){
    time = newArray;
  }
  
  void setPrevTime(int index){
    prevTime[index] = time[index];
  }
  
  boolean[] getPrevTimeArray(){
    return prevTime;
  }
  
  
  boolean getFalseValues(){
    int falseCounter = 0;

     for( int j= 0; j<time.length; ++j){
       if (!time[j]){
         ++falseCounter;}
     }
     if (falseCounter == 16) return true;
      else
      return false;

  }
  
  int getTrueValues(){
    int trueCounter = 0;
    for( int j= 0; j<time.length; ++j){
       if (time[j]){
         ++trueCounter;}
     }
    return trueCounter;
  }
  
   public String toString() {
    String message="";
    if (id == -1) 
      message = "Obj " + id;
    else 
     message = "Obj  " + id;
    return message;
  }

}