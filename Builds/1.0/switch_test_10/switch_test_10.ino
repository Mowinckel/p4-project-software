//leds for waveform
const int numberLeds = 4;
int led[numberLeds] = {13, 12, 11, 10};
int ledState[numberLeds];

//pin for potentiometer
int potPin = A0;
//potentiometers value
int val = 0;

//Pins for each LED
//int led[waveformLeds] = {7, 5};
//State of each LED
//int ledState[waveformLeds];
const int numberNotes = 6;
int notes[numberNotes] = {7, 6, 5, 4, 3, 2};

int buttonUp = 24;
int buttonDown = 25;

int buttonUndo = 23;

int dyaLed = 14;
int triLed = 16;
int dyadButton = 26;
int triadButton = 27;

int counter = 0;

int channel = 0;

int chord = 0;

boolean undoPressed = false;

boolean led1ON = false;
boolean led2ON = false;

//Number of buttons
//const int waveformButtons = 2;

//Set the pins used for each button
//int button[waveformButtons] = {6, 4};

//Start state of the LEDs off (HIGH=off when INPUT_PULLUP used)
//No need for pullup/down resistors
//int lastButtonState[waveformButtons];

//Bools used for when buttons currently pressed (used for long press detection)
//bool buttonActive[waveformButtons];

//Debounce timer for each button
/*unsigned long lastDebounceTime[waveformButtons];*/


//Set the debounce delay
int debounceDelay = 25;
//Set the time required for a long press
//int longPressDelay = 1;

//Counter array for when note was played in timeline
int playedPos[] = {0, 0, 0, 0, 0, 0, 0, 0};

//The lastButtonState used detecting note button release
//int lastNoteState = 1;
int lastNoteState[numberNotes];
//int buttonState = 1;
int buttonState[numberNotes] = {-1, -1, -1, -1, -1, -1};

String message = "";
String boolMsg = "";

int startPos = 0;

bool startCount = false;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
//  buttonSetup();
//  ledSetup();
//  buttonNoteSetup();

  pinMode(led[0],OUTPUT);
  pinMode(led[1],OUTPUT);
  pinMode(led[2],OUTPUT);
  pinMode(led[3],OUTPUT);

  pinMode(notes[0], INPUT_PULLUP);
  pinMode(notes[1], INPUT_PULLUP);
  pinMode(notes[2], INPUT_PULLUP);
  pinMode(notes[3], INPUT_PULLUP);
  pinMode(notes[4], INPUT_PULLUP);
  pinMode(notes[5], INPUT_PULLUP);

  pinMode(buttonUp, INPUT_PULLUP);
  pinMode(buttonDown, INPUT_PULLUP);

  pinMode(buttonUndo, INPUT_PULLUP);

  pinMode(dyaLed, OUTPUT);
  pinMode(triLed, OUTPUT);
  pinMode(dyadButton, INPUT_PULLUP);
  pinMode(triadButton, INPUT_PULLUP);
}

void loop() {
  // put your main code here, to run repeatedly:

  //Loop through each button and get the state
  //0=not pressed
  //1=short press
  //2=long press
  //You could also loop through, set veriables for each button
  //then act on each buttons state seperately
  triadicButton();
  waveformButton();
  sendNavigation();
  sendNote();
  sendPressedArray();
  sendUndo();
  

  for(int y=0;y<numberLeds;y++){    
      int ledState = digitalRead(led[y]);
      //Serial.print(ledState);
  }
  //Serial.println("");

  
  if (counter == 7) {
    counter = -1;
  }
  if (startCount) {
    counter++;
    delay(500);
  }
  
  //count();
}

void waveformButton() {
    val = analogRead(potPin);
  //val = map(val, 0, 1023, 0, 4);

  if (val <= 256){
    digitalWrite(led[0], HIGH);
    digitalWrite(led[1], LOW);
    digitalWrite(led[2], LOW);
    digitalWrite(led[3], LOW);
  }
  else if (val > 256 && val <= 512){
    digitalWrite(led[0], LOW);
    digitalWrite(led[1], HIGH);
    digitalWrite(led[2], LOW);
    digitalWrite(led[3], LOW);
  }
  else if (val > 512 && val <= 768){
    digitalWrite(led[0], LOW);
    digitalWrite(led[1], LOW);
    digitalWrite(led[2], HIGH);
    digitalWrite(led[3], LOW);
  }
  else {
    digitalWrite(led[0], LOW);
    digitalWrite(led[1], LOW);
    digitalWrite(led[2], LOW);
    digitalWrite(led[3], HIGH);
  }
}

void triadicButton() {
  int button1State = digitalRead(dyadButton);
  int button2State = digitalRead(triadButton);
  int led1State = digitalRead(dyaLed);
  int led2State = digitalRead(triLed);
  
  if (button1State == LOW && led1State == LOW) {
    led1ON = true;
    chord = 1;
  }
  if (button2State == LOW && led2State == LOW) {
    led1ON = true;
    led2ON = true;
    chord = 2;
  }  

  if (button1State == LOW && led1State == HIGH) {
    led1ON = false;
    led2ON = false;
    chord = 0;
  }

  if (button2State == LOW && led2State == HIGH) {
    led2ON = false;
    chord = 1;
  }

  if (led1ON) {
    digitalWrite(dyaLed, HIGH);
  }
  else
    digitalWrite(dyaLed, LOW);

  if (led2ON)
    digitalWrite(triLed, HIGH);
  else
    digitalWrite(triLed, LOW);
}

void sendNote() {
  
  for(int y=0;y<numberLeds;y++){
    for(int x=0;x<numberNotes;x++){
    
      buttonState[x] = digitalRead(notes[x]);
      int ledState = digitalRead(led[y]);
   
      if (buttonState[x] == LOW && ledState == HIGH) {
        startCount = true;
        message = String(buttonState[x] + x) + "," + String(y) + "," + String(channel) + "," + String(chord);
        playedPos[counter] = 1;
        if (x+1 == numberNotes) {
          message = "n"; 
          sendBeat();
        }
        else {
          Serial.println(message);
        }
      }    
    }
  }
}

void sendPressedArray () {
  for (int t = 0; t < numberNotes; t++){
    if (buttonState[t] != lastNoteState[t]) {
      if (buttonState[t] == HIGH) {
        if (t != numberNotes-1) {
          String playerPosMsg;
          for (int i = 0; i < 8; i++) {
            if (i < 7)
              playerPosMsg = playerPosMsg + playedPos[i] + ",";
            else
              playerPosMsg = playerPosMsg + playedPos[i];
          }
          
          //Print the final message to serial
          playerPosMsg = message + "," + playerPosMsg;
          if (message !=  "")
            Serial.println(playerPosMsg);
          
          for (int i = 0; i < 8; i++) {
            playedPos[i] = 0;
          }
        }
      }
    }
    lastNoteState[t] = buttonState[t];
   }
}

void sendBeat () {
  String playerPosMsg;
  boolean checked = false;
  for (int i = 0; i < 8; i++) {
    if (i < 7)
      playerPosMsg = playerPosMsg + playedPos[i] + ",";
    else
      playerPosMsg = playerPosMsg + playedPos[i];
  }
    
    //Print the final message to serial
    playerPosMsg = message + "," + playerPosMsg;
    int falseValues = 0;
    for (int i = 0; i < 8; i++) {
      if (playedPos[i] == 0) {
        falseValues++;
      }
    }
    if (falseValues != 8)
      Serial.println(playerPosMsg);
      
    for (int i = 0; i < 8; i++) {
      playedPos[i] = 0;
    }
}

void sendNavigation(){
  
  int buttonUpState = digitalRead(buttonUp);
  int buttonDownState = digitalRead(buttonDown);
  
  if (buttonUpState == LOW)
    channel++;
 
  if (buttonDownState == LOW)
    channel--;

  if (channel > 2)
    channel = 0;

  if (channel < 0)
    channel = 2;
}

void sendUndo () {
  int buttonUndoState = digitalRead(buttonUndo);
  
  if (buttonUndoState == LOW && !undoPressed) { 
    Serial.println("u");
    undoPressed = true;
  }
  if (buttonUndoState == HIGH)
    undoPressed = false;
}
