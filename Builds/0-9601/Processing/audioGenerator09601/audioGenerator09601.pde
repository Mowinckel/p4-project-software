import processing.serial.*;
import ddf.minim.*;
import ddf.minim.ugens.*;
import java.util.ArrayList;
import hypermedia.net.*;

int m;
int n;
//Global variables
//These variables are for serial communication
UDP udp;
int port = 12001;
String hostIP = "127.0.0.1";

//commented out temporarily, for testing purposes
//boolean pressed = false; 
boolean pressed = true;

boolean[] frameToBoolean = new boolean[16];
// this is the list of input notes
ArrayList<Input> inputObjects = new ArrayList<Input>();
Input tempInput = new Input(-1);
int id;
int note;
int waveform;

Serial myPort;

//the number of elements in this array should be shortened
ADSR[] adsr = new ADSR[12];

//These are for minim audio
AudioOutput aOut;
Minim minim;

//Input objects and arrays
Input[][] soundLoop = new Input[16][4]; // [16] = 4 seconds [4] the 4 different channels
boolean[] dummyBoolArray = new boolean[16];
Input dummy = new Input(-1, -1, -1, -1, -1, dummyBoolArray, dummyBoolArray);

//Oscillators - used to avoid NullPointerException for playTemporary()
Oscil tempOsc = new Oscil(0,0,Waves.SINE);


//General variables used in the program
boolean alreadyPlayed = false;

String[] parts = {};

Input firstInput = null;

boolean playing = true;

boolean startLoop = false;

//String val="";
//Testing
//3rd channel only for noise
String val = "";  //note, waveform, channel, chord, rest are boolean values
//String val =     "3, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, ";  //note, waveform, channel, chord, rest are boolean values
//String valTest1 = "2, 0, 1, 2, 1, 1, 0, 0, 0, 0, 0, 0, ";
//String valTest1 = "n, 1, 0, 0, 0, 0, 0, 0, 0, ";
String valTest2 = "u";
String valTest1 = "2, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, ";

void setup() {
  minim = new Minim(this);
  
  udp = new UDP(this, port, hostIP);
  udp.log(true);
  udp.listen(true);
  
  aOut = minim.getLineOut();
  for (int i = 0; i < adsr.length; ++i)
    adsr[i] = new ADSR(0.0, 0.0, 0.0, 0.0, 0.0);
    
  createSoundArray(inputObjects);
  print2Darray();
  thread("playSoundArray");
 // frameRate(2.5); //This equals to 150BPM

}

void draw() {
  while (pressed) {
     println("pressed is true");
     splitMessage(val);
     if (parts.length == 2) {
       startLoop = true;
       thread("playIncoming");
     } //<>//
     if (parts.length > 2 || parts.length == 1) {
       println("Parts > 2 or == 1");
       adsr[adsr.length-1].noteOff();
       playing = true;
       overrideInputObjects();
       createSoundArray(inputObjects);
       print2Darray();
     } else if (parts.length <= 0) {
       println("message is nothing");
       parts = new String[0];
     }
     pressed = false;
   }
}

//For testing purposes only
void print2Darray(){
   for (int i = 0; i < soundLoop.length; i++) {
       for (int j = 0; j < soundLoop[i].length; j++) {
         print(soundLoop[i][j] + " ");
       }
       println("");
     }
     println("");
}

void createSoundArray (ArrayList<Input> inputs) {
  //Store values in a 2D array with x = channels, y = i (current frame)
  for (int i = 0; i < soundLoop.length; i++) {
     for (int j = 0; j < soundLoop[i].length; j++) {
       for (int k = 0; k < inputs.size(); k++) {
         if (j == inputs.get(k).getChannel() && inputs.get(k).getTime(i)) {
           soundLoop[i][j] = inputs.get(k);
         }
       }
       if (soundLoop[i][j] == null)
         soundLoop[i][j] = dummy;
     }
  }
}

//Delay time has a fixed value - 1 note has a time of 0.5 seconds
void playSoundArray () {
  while (true) {
    println("Playing soundarray");
    for (int i = 0; i < soundLoop.length; i++) {  
      for (int j = 0; j < soundLoop[i].length; j++) {
        int beforeI = i-1;
        if (beforeI < 0)  beforeI = soundLoop.length-1;
        int afterI = i+1;
        if (afterI == soundLoop.length) afterI = 0;   
      
      //start playing the sound
       if (soundLoop[i][j].getID() != -1 && (soundLoop[i][j].getID() != soundLoop[beforeI][j].getID())) {
         //if in noise channel
         if(soundLoop[i][j].getChannel() == 3){
           Noise testNoise = new Noise(1.5f, Noise.Tint.WHITE);
           adsr[j] = new ADSR(1.5, 0.05, 0.15, 0.1, 0.05);
           testNoise.patch(adsr[j]);
         }
         //if in other channel
         else{
           Oscil mainOsc = new Oscil(soundLoop[i][j].getFrequency(), 0.8f, soundLoop[i][j].getWaveform());
           adsr[j] = new ADSR(0.6, 0.04, 0.09, 0.1, 0.1);
           mainOsc.patch(adsr[j]);
         }
             adsr[j].patch(aOut);
             adsr[j].noteOn();
             
             //for dyad
             if (soundLoop[i][j].getChord() != 0){
               int newJ = j+4; //<>//
               adsr[newJ] = new ADSR(0.6, 0.04, 0.09, 0.1, 0.1);
               Oscil secondOsc = new Oscil(soundLoop[i][j].getDyad(), 0.8f, soundLoop[i][j].getWaveform());
               secondOsc.patch(adsr[newJ]);
               adsr[newJ].patch(aOut); //<>//
               adsr[newJ].noteOn();
             
               //for triad
               if (soundLoop[i][j].getChord() == 2){
                  newJ = j+8;
                  adsr[newJ] = new ADSR(0.6, 0.04, 0.09, 0.1, 0.1);
                  Oscil thirdOsc = new Oscil(soundLoop[i][j].getTriad(), 0.8f, soundLoop[i][j].getWaveform());
                  thirdOsc.patch(adsr[newJ]);
                  adsr[newJ].patch(aOut);
                  adsr[newJ].noteOn();
               }
             }  //<>//
             println("Playing " + soundLoop[i][j].getID());
              m = millis();
              println(m);
       }
       
       
    
    //start the release of the sound envelope
        else if (soundLoop[i][j].getID() != -1 && (soundLoop[i][j].getID() != soundLoop[afterI][j].getID())) {
           adsr[j].noteOff();
           if (soundLoop[i][j].getChord() != 0){
             int newJ = j+4;
             adsr[newJ].noteOff();
             if (soundLoop[i][j].getChord()== 2){
                  newJ = j+8;
                  adsr[newJ].noteOff();
             }
           }
            println("Releasing " + soundLoop[i][j].getID());
         }
       }
       delay(250);
    }
  }
}

//put boolean input from arduino into boolean array of an Input object
void makeFrames(int value, String[] parts){
  int j = 0;
  for (int i = value; i < parts.length; i++){
    int k = j+1;
    if (parts[i].trim().indexOf('1') >= 0){
      frameToBoolean[j] = true;
      frameToBoolean[k] = true;
    }
    j = j+2;
  }
}

void splitMessage(String message) {
  if (message != "") {
    parts = split(message,','); 
    frameToBoolean = new boolean[16];
   
    //if the "Undo" button is pressed
    if (parts[0].trim().indexOf('u') >= 0){
      if (inputObjects.size() > 0){
        tempInput.setID(-1); //<>//
       
        //a bit hard coded for now
        //returns the value of the previous object
        if (inputObjects.size() > 1 && inputObjects.get(inputObjects.size()-2).getID() != -1)
          inputObjects.get(inputObjects.size()-2).setTimeArray(inputObjects.get(inputObjects.size()-2).getPrevTimeArray());
          
        inputObjects.remove(inputObjects.get(inputObjects.size()-1));
      }
      return;
    }
    
    //if the noise button is pressed
    if (parts[0].trim().indexOf('n') >= 0){
      println("Received beat");
       makeFrames(1, parts);
       tempInput = new Input(id, 3, 0, frameToBoolean);
       id++;
    }
    else {
    note = Integer.parseInt(parts[0].trim()); 
    waveform = Integer.parseInt(parts[1].trim());
    tempInput = new Input(-1, note, waveform);
    if (parts.length != 2){
      int channel = Integer.parseInt(parts[2].trim());
      int chord = Integer.parseInt(parts[3].trim());
      makeFrames(4, parts);
      tempInput = new Input(id, note, waveform, channel, chord, frameToBoolean, frameToBoolean);
      ++id;
    }
      
    }
  }
  else {
    
  }
  //println("Length of parts: " + parts.length);
 }
  

 void overrideInputObjects(){
   for( int i= 0; i< inputObjects.size(); ++i){
      if (tempInput.getChannel() == inputObjects.get(i).getChannel()){ 
        for( int j= 0; j<frameToBoolean.length; ++j){
         if (inputObjects.get(i).getTime(j) && tempInput.getTime(j)){
            inputObjects.get(i).setTimeToFalse(inputObjects.get(i).getTime(j));  //<>//
        }   
      }
   }
   /*
   //make function remove those objects, that have only false values in them
   if (inputObjects.get(i).getFalseValues()){
          inputObjects.remove(inputObjects.get(i));
      }*/
   }
   if (tempInput.getID() != -1)
     inputObjects.add(tempInput);  
 }
 
void receive(byte[] data, String ip, int port) {
  String message = new String(data);
  val = message;
  pressed = true;
  println("Received: " + message);
  println("Pressed is: " + pressed);
}

void playIncoming () {
  //TODO: have a thing for noise button
  if (playing) {
    println("Playing temp");
    Oscil tOsc = new Oscil(tempInput.getFrequency(), 0.8f, tempInput.getWaveform());
    adsr[adsr.length-1] = new ADSR(0.6, 0.04, 0.09, 0.1, 0.1);
    tOsc.patch(adsr[adsr.length-1]);
    adsr[adsr.length-1].patch(aOut);
    adsr[adsr.length-1].noteOn();
    //firstInput = tempInput;
    playing = false;
    m = millis();
    println("Initial time" + m);
  }
  /*adsr[adsr.length-1].noteOff();
  firstInput = null;*/
  
}