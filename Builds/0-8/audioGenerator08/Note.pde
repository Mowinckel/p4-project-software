class Input {
  private int id;
  private int frequency;
  private int waveform;
  private int channel;
  private int chord;
  private boolean[] time;
  
  Input () {
    id = 0; //Consider writing a better default constructor
  }
  
  Input (int tempFrequency, int tempWaveform) {
    frequency = tempFrequency;
    waveform = tempWaveform;
  }
  
  
  Input (int tempId, int tempFrequency, int tempWaveform, int tempChannel, int tempChord, boolean[] tempTime) {
    frequency = tempFrequency;
    waveform = tempWaveform;
    channel = tempChannel;
    time = tempTime;
    id = tempId;
    chord = tempChord;
  }
  
  //for noise
  Input (int tempId, int tempChannel, int tempChord, boolean[] tempTime) {
    channel = tempChannel;
    time = tempTime;
    id = tempId;
    chord = tempChord;
  }
  
  int getChord(){
    return chord;
  }
  
 void  setTimeToFalse(int newTime) {
  this.time[newTime] = false;
  }
  
  int getID () {
    return id;
  }
  
  void setID (int newID){
    id = newID;
  }
  
  float getFrequency () {
    switch (frequency) {

     case 0: return 261.63; //C4
      case 1: return 293.66; //D4
      case 2: return 392.00; //G4
      case 3: return 440.00; //A4
      
      default: return 440.00; //A4
    }
  }
   float getDyad() {
     switch (frequency) {
      case 0: return 329.6; //E4
      case 1: return 370.0; //F#4
      case 2: return 493.9; //B4
      case 3: return 554.4; //C#5
      
      default: return 440.00; //A4
     }
   }
  
  float getTriad() {
    switch (frequency) {
      case 0:  return 493.88; //B4
      case 1:  return 554.37; //C#5
      case 2: return 739.99; //F#5
      case 3: return 830.61; //G#5
      
      default: return 440.00; //A4
     }
  }
  
  
  Wavetable getWaveform () {

   switch (waveform) {
      case 0: return Waves.SQUARE;
      case 1: return Waves.TRIANGLE;
      case 2: return Waves.SAW;

     case 3: return Waves.QUARTERPULSE;
      default: return Waves.SQUARE;
    }
  }
  
  int getChannel () {
    return channel;
  }
  
  boolean getTime (int index) {
    return time[index];
  }
  
  boolean getFalseValues(){
    int falseCounter = 0;

     for( int j= 0; j<time.length; ++j){
       if (!time[j]){
         ++falseCounter;}
     }
     if (falseCounter == 16) return true;
      else
      return false;

  }
  
  int getTrueValues(){
    int trueCounter = 0;
    for( int j= 0; j<time.length; ++j){
       if (time[j]){
         ++trueCounter;}
     }
    return trueCounter;
  }
  
   public String toString() {
    String message="";
    if (id == -1) 
      message = "Obj " + id;
    else 
     message = "Obj  " + id;
    return message;
  }

}