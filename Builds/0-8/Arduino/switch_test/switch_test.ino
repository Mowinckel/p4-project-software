//Number of Waveform LEDs
const int waveformLeds = 2;
//Pins for each LED
int led[waveformLeds] = {7, 5};
//State of each LED
int ledState[waveformLeds];
const int numberNotes = 5;
int notes[numberNotes] = {12, 11, 10, 9, 8};

int counter = 0;

//Number of buttons
const int waveformButtons = 2;

//Set the pins used for each button
int button[waveformButtons] = {6, 4};

//Start state of the LEDs off (HIGH=off when INPUT_PULLUP used)
//No need for pullup/down resistors
int lastButtonState[waveformButtons];

//Bools used for when buttons currently pressed (used for long press detection)
bool buttonActive[waveformButtons];

//Debounce timer for each button
unsigned long lastDebounceTime[waveformButtons];


//Set the debounce delay
int debounceDelay = 25;
//Set the time required for a long press
//int longPressDelay = 1;

//Counter array for when note was played in timeline
int playedPos[] = {0, 0, 0, 0, 0, 0, 0, 0};

//The lastButtonState used detecting note button release
//int lastNoteState = 1;
int lastNoteState[4];
//int buttonState = 1;
int buttonState[4] = {-1, -1, -1, -1};

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  buttonSetup();
  ledSetup();
  buttonNoteSetup();

}

void loop() {
  // put your main code here, to run repeatedly:

  //Loop through each button and get the state
  //0=not pressed
  //1=short press
  //2=long press
  //You could also loop through, set veriables for each button
  //then act on each buttons state seperately
  iterator();
  sendNote();
  //Serial.print("ButtonState after sendNote: ");
  //Serial.println(buttonState);

for (int t = 0; t < 4; t++){
  if (buttonState[t] != lastNoteState[t]) {
    Serial.println("Two states are different");
    if (buttonState[t] == HIGH) {
      Serial.println("Button went from on to off");
      String playerPosMsg;
      for (int i = 0; i < 8; i++) {
        playerPosMsg = playerPosMsg + playedPos[i] + ",";
      }
      Serial.println(playerPosMsg);
      for (int i = 0; i < 8; i++) {
        playedPos[i] = 0;
    }
    }
  }
    lastNoteState[t] = buttonState[t];
    
}

  
 

  delay(500);
  if (counter == 7) {
    counter = -1;
  }
  counter++;
  //count();
}

int buttonHandler(int number)
{
  //Handle presses for each button
  int reading = digitalRead(button[number]);

  //Check if button state has changed since last check
  if (reading != lastButtonState[number])
  {
    if (reading == HIGH && !buttonActive[number])
    {
      lastButtonState[number] = reading;

      //Return 0 (not pressed)
      return 0;
    }

    //if reading is high (open)
    if (reading == HIGH && buttonActive[number])
    {
      if (millis() - lastDebounceTime[number] > debounceDelay)
      {
        lastButtonState[number] = reading;

        buttonActive[number] = true;

        //Return 1 (short press)
        return 1;
      }

      lastButtonState[number] = reading;

      buttonActive[number] = false;

      //Return 0 (not pressed)
      return 0;
    }

    //if reading is low (closed)
    else if (reading == LOW)
    {
      if (!buttonActive[number])
      {
        //Start debounce timer
        lastDebounceTime[number] = millis();

        lastButtonState[number] = reading;

        buttonActive[number] = true;

        //Return 0 (not pressed)
        return 0;
      }
      //Return 0 (not pressed)
      return 0;
    }
  }

  //Check if reading still high (open)
  if (reading == HIGH)
  {
    lastButtonState[number] = reading;

    buttonActive[number] = false;

    //Return 0 (not pressed)
    return 0;
  }



}
void ledSetup()
{
  for (int x = 0; x < waveformLeds; x++)
  {
    pinMode(led[x], OUTPUT);
    ledState[x] = LOW;
  }
}

void buttonSetup()
{
  for (int x = 0; x < waveformButtons; x++)
  {
    lastButtonState[x] = HIGH;
    buttonActive[x] = false;
    lastDebounceTime[x] = 0;
    // buttonState[x]=HIGH;
    pinMode(button[x], INPUT_PULLUP); //Maybe change to normal INPUT

  }
}

void iterator() {

  for (int x = 0; x < waveformButtons; x++)
  {
    int state = buttonHandler(x);

    if ( state == 1)
    {
      ledState[x] = !ledState[x];
      digitalWrite(led[x], ledState[x]);
    }
  }
}

void buttonNoteSetup() {
  for (int x = 0; x < numberNotes; x++)
  {
    lastDebounceTime[x] = 0;
    pinMode(notes[x], INPUT_PULLUP);
  }
}

void sendNote() {
  for (int y = 0; y < waveformLeds; y++) {
    for (int x = 0; x < numberNotes; x++) {
    
      buttonState[x] = digitalRead(notes[x]);
     
      //Serial.print("ButtonState in sendNote before pressed: ");
      //Serial.println(buttonState);
      String message = String(buttonState[x] + x) + "," + String(y);
      if (buttonState[x] == LOW && ledState[y] == HIGH) {
        
        //Serial.print("ButtonState in sendNote while pressed: ");
        //Serial.println(buttonState);
        playedPos[counter] = 1;
        Serial.println(message);
      }
    
    }
  }
}

