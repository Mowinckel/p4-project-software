﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class SpawnNew : MonoBehaviour {

    //Used to identify different spawning points aka rings
    public int id = 0;
    //what ring the user has selected
    public int idSelector = 1;
	public Vector3 current;
    public Vector3 beatCurrent;
    public Transform Target;
    public GameObject NoteG;
    public GameObject NoteA;
    public GameObject NoteD;
    public GameObject NoteC;
    public GameObject Beat;
    public GameObject oldPlanetClone = null;
	public GameObject gm;

	GameObject tempPlanet;
	GameObject previousPlanet;
	public GameObject squareWaveformer;
	public GameObject pulseWaveformer;
	public GameObject sawtoothWaveformer;
    public GameObject triangleWaveformer;
	private Transform tempMoon;
	private Transform previousMoon;
	private Transform tempWave;
	private Transform previousWave;

	private Transform moving;
	private float lifetime;
	private GameObject waveSelector;

    public SnappointPosition snapPosition;
	List <Planet> planetList = new List<Planet> ();

    // Use this for initialization
    void Start () {
		snapPosition = gameObject.GetComponent<SnappointPosition> ();
		waveSelector = triangleWaveformer;
	}
	
	// Update is called once per frame
	void Update () {
		idSelector = gm.GetComponent<GameManager> ().channel;
		Debug.Log ("ID Selector(SN): " + idSelector);
		current = snapPosition.currentSnap;
        beatCurrent = snapPosition.beatSnap;
        int length = planetList.Count;

		/*
=======

		if (Input.GetKeyDown (KeyCode.Keypad0)) {
			waveSelector = squareWaveformer;
		}
		if (Input.GetKeyDown (KeyCode.Keypad1)) {
			waveSelector = triangleWaveformer;
		}
		if (Input.GetKeyDown (KeyCode.Keypad2)) {
			waveSelector = pulseWaveformer;
		}
		if (Input.GetKeyDown (KeyCode.Keypad3)) {
			waveSelector = sawtoothWaveformer;
		}

>>>>>>> 16363f241d58d07967996cb9d332b56c02334c4e
        //selecting rings using up and down arrow keys
        if (Input.GetKeyDown(KeyCode.UpArrow))
            --idSelector;
        
        if (Input.GetKeyDown(KeyCode.DownArrow))
            ++idSelector;

        if (idSelector < 0)
            idSelector = 2;
        if (idSelector > 2)
            idSelector = 0;
		*/
		if (Input.GetKeyDown (KeyCode.B)) 
		{
			SpawnBeat ();
		}


        /*
=======
        if (Input.GetKeyDown(KeyCode.B))
        {
            Instantiate(Beat, beatCurrent, Target.rotation);
            Planet planet = new Planet(snapPosition.ring, snapPosition.pos);
        }

>>>>>>> 16363f241d58d07967996cb9d332b56c02334c4e
        if (idSelector == 1 && id == 1)
        {
            if (Input.GetKeyDown(KeyCode.G)) {

				addToArrayList (NoteG);
            }
            if (Input.GetKeyDown(KeyCode.A))
            {
				addToArrayList (NoteA);
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
				addToArrayList (NoteD);
            }
            if (Input.GetKeyDown(KeyCode.C))
            {
				addToArrayList (NoteC);
            }
        }

        if (idSelector == 2 && id == 2)
        {

            if (Input.GetKeyDown(KeyCode.G))
            {
				addToArrayList (NoteG);
            }
            if (Input.GetKeyDown(KeyCode.A))
            {
				addToArrayList (NoteA);
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
				addToArrayList (NoteD);
            }
            if (Input.GetKeyDown(KeyCode.C))
            {
				addToArrayList (NoteC);
            }
        }

        if (idSelector == 0 && id == 0)
        {

            if (Input.GetKeyDown(KeyCode.G))
            {
				addToArrayList (NoteG);
            }
            if (Input.GetKeyDown(KeyCode.A))
            {
				addToArrayList (NoteA);
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
				addToArrayList (NoteD);
            }
            if (Input.GetKeyDown(KeyCode.C))
            {
				addToArrayList (NoteC);
            }
        }
		*/
        if (Input.GetKeyDown(KeyCode.P))
        {
			int tempPos = planetList[length-1].GetPos();
			int tempRing = planetList[length-1].GetRing();
			GameObject oldPlanet = GameObject.Find("Planet " + tempPos + tempRing);

			if (oldPlanetClone != null)
            {
				Destroy(oldPlanet);
				foreach (Renderer r in oldPlanetClone.GetComponentsInChildren<Renderer>())
					r.enabled = true;
				oldPlanetClone.name = "Planet " + tempPos + tempRing;
				oldPlanetClone = null;
				Destroy (oldPlanetClone);
            }

			if (oldPlanetClone == null)
			{
				Destroy(oldPlanet);
			}
        }
    }

	void addToArrayList (GameObject note)
	{
		GameObject newNote = note;
        GameObject waveInstance;
        GameObject objPreviousMoon;
        Transform finalPlanet;
        GameObject objFinalPlanet;
        Planet planet = new Planet(snapPosition.ring, snapPosition.pos);
        int newPos = planet.GetPos();
        int newRing = planet.GetRing();
        int length = planetList.Count;
        int modifier = -1;
		Destroy(oldPlanetClone);
        for (int i = 0; i < length; i++)
        {
            int tempPos = planetList[i].GetPos();
            int tempRing = planetList[i].GetRing();
            GameObject oldPlanet = GameObject.Find("Planet " + tempPos + tempRing);
            if (length != 0)
            {
                if (tempPos == newPos && tempRing == newRing)
                {
                    planetList.Remove(planetList[i]);
					moving = oldPlanet.transform;
					Vector3 movingSnap = moving.transform.position;
					oldPlanetClone = Instantiate (oldPlanet, movingSnap, Target.rotation) as GameObject;
					foreach (Renderer r in oldPlanetClone.GetComponentsInChildren<Renderer>())
						r.enabled = false;
                    Destroy (oldPlanet);
                    break;
                }
            }
        }
        planetList.Add(planet);
        tempPlanet = (GameObject)Instantiate (newNote, current, Target.rotation);
        Renderer tren = tempPlanet.GetComponent<Renderer>();
        tren.enabled = false;
        SpriteRenderer tsren = tempPlanet.GetComponent<SpriteRenderer>();
        tsren.enabled = false;
		tempPlanet = Instantiate (newNote, current, Target.rotation) as GameObject;
        tempPlanet.name = "Planet " + newPos + newRing;
        waveInstance = Instantiate(waveSelector, new Vector3(-4.1f, 0.0f, 0.0f), Quaternion.Euler(0, -90, 0)) as GameObject;
        waveInstance.transform.SetParent(tempPlanet.transform, false);
        waveInstance.name = "waveform";
        int previousPos = newPos - 1;
        if (previousPos < 0)
            previousPos = 7;

        for (int i = 0; i < 100; i++) 
        {
            try
            {
                previousPlanet = GameObject.Find("Planet " + previousPos + newRing);
            }
            catch
            {
                previousPlanet = null;
            }
            if (previousPlanet != null)
            {
                tempMoon = tempPlanet.transform.Find("moon");
                previousMoon = previousPlanet.transform.Find("moon");
                tempWave = tempPlanet.transform.Find("waveform");
                previousWave = previousPlanet.transform.Find("waveform");
                Debug.Log(tempMoon.tag + " " + previousMoon.tag + " " + tempWave.tag + " " + previousWave.tag);
                objPreviousMoon = previousMoon.gameObject;
                if (previousPos < 0)
                    previousPos = 7;
		previousPos = newPos - 1;
		previousPlanet = GameObject.Find ("Planet " + previousPos + newRing);
		tempMoon = tempPlanet.transform.Find ("moon");
		previousMoon = previousPlanet.transform.Find ("moon");
//		tempWave = tempPlanet.FindWithTag ("Waveform");
//		previousWave = previousPlanet.FindWithTag ("Waveform");


                if (tempMoon.tag == previousMoon.tag && tempWave.tag == previousWave.tag)
                {
                    Debug.Log("A planet is identical");
                    Renderer ren = objPreviousMoon.GetComponent<Renderer>();
                    ren.enabled = false;
                    SpriteRenderer sren = objPreviousMoon.GetComponent<SpriteRenderer>();
                    sren.enabled = false;
                }
            }
            if (previousPlanet == null)
            {
                Debug.Log("No previous planet.");
                if (previousPlanet == null && modifier == +1) {
                    Debug.Log("this is actually happening");
                    previousPos = previousPos - 1;
                    if (previousPos < 0)
                        previousPos = 7; 
                    previousPlanet = GameObject.Find("Planet " + previousPos + newRing);
                    finalPlanet = previousPlanet.transform.Find("moon");
                    objFinalPlanet = finalPlanet.gameObject;
                    Renderer ren = finalPlanet.GetComponent<Renderer>();
                    ren.enabled = true;
                    SpriteRenderer sren = finalPlanet.GetComponent<SpriteRenderer>();
                    sren.enabled = true;
                    break;
            }
                modifier = +1;
            }
            previousPos = previousPos + modifier;
            if (previousPos < 0)
                previousPos = 7;
            if (previousPos > 7)
                previousPos = 0;
        }
			
		if (previousPlanet == null) {
			Debug.Log ("No previous planet.");
			// Add invisible planet with the tail
		} 

	}
		
	public void SpawnBeat () {
		Debug.Log ("Spawning beat");
		beatCurrent = snapPosition.beatSnap;
		Instantiate(Beat, beatCurrent, Target.rotation);
		Planet planet = new Planet(snapPosition.ring, snapPosition.pos);
	}

	public void SpawnPlanet (int note) {
		Debug.Log ("ID Selector(SN) before change: " + idSelector);

		Debug.Log ("ID Selector(SN) after change: " + idSelector);
		current = snapPosition.currentSnap;
		beatCurrent = snapPosition.beatSnap;
		int length = planetList.Count;

		//selecting rings using up and down arrow keys
		
		if (idSelector == 0 && id == 0) {
			AddFromNote (note);
		}

		if (idSelector == 1 && id == 1) {
			AddFromNote (note);
		}

		if (idSelector == 2 && id == 2) {
			AddFromNote (note);
		}


	}

	void AddFromNote (int note) {
		switch (note) {
		case 0: 
			addToArrayList (NoteC);
			break;
		case 1:
			addToArrayList (NoteD);
			break;
		case 2: 
			addToArrayList (NoteG);
			break;
		case 3:
			addToArrayList (NoteA);
			break;
		}
	}
}

class Planet
{
    private int ring;
    private int pos;

    public Planet(int tempRing, int tempPos)
    {
        ring = tempRing;
        pos = tempPos;
    }

    public int GetRing()
    {
        return ring;
    }
    public int GetPos()
    {
        return pos;
    }
}
