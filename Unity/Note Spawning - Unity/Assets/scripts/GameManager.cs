﻿using UnityEngine;
using System.Collections;
using System.IO.Ports;
using System;

public class GameManager : MonoBehaviour {

	SerialPort stream = new SerialPort("COM4", 9600);
	string incMessage;
	public string tempMessage;
	public bool spawning = false;
	int note = -1;
	int waveform;
	public int channel;
	int chord;
	ParticleSystemRenderer psr;
	GameObject spawnedPlanet;
	GameObject spawnedWaveformer;
	public int idSelector = 1;

	public GameObject NoteCPlanet;
	public GameObject NoteDPlanet;
	public GameObject NoteGPlanet;
	public GameObject NoteAPlanet;

	public GameObject squareWave;
	public GameObject triangleWave;

	public Transform spawnPoint;

	public GameObject spawner0;
	public GameObject spawner1;
	public GameObject spawner2;
	public GameObject spawner3;
	private SpawnNew spawnScript;

	// Use this for initialization
	void Start () {
		stream.ReadTimeout = 50;
		stream.Open ();
	}

	// Update is called once per frame
	void Update () {
		StartCoroutine (
			AsynchronousReadFromArduino (
				(string s) => Debug.Log (s),
				() => Debug.LogError ("Error!"),
				0.5f
			)
		);
		/*if (Input.GetKeyDown(KeyCode.UpArrow))
			--idSelector;

		if (Input.GetKeyDown(KeyCode.DownArrow))
			++idSelector;

		if (idSelector < 0)
			idSelector = 2;
		if (idSelector > 2)
			idSelector = 0;*/

		Debug.Log ("ID Selector(GM): " + idSelector);
	}

	public IEnumerator AsynchronousReadFromArduino(Action<string> callback, Action fail = null, float timeout = float.PositiveInfinity) {
		DateTime initialTime = DateTime.Now;
		DateTime nowTime;
		TimeSpan diff = default(TimeSpan);

		string dataString = null;

		do {
			try {
				dataString = stream.ReadLine ();
			} catch (TimeoutException) {
				dataString = null;
			}

			if (dataString != null) {
				incMessage = dataString;
				callback (dataString);
				SplitMessage();
				Debug.Log("Note: " + note + " , Waveform: " + waveform + " , Channel: " + channel + ", Chord: " + chord);
				if (note != -1) {
					Debug.Log("Note is not -1");
					SpawnPlanet();
				}
				spawning = true;
				yield return null;
			} else
				spawning = false;
				yield return new WaitForSeconds (0.05f);

			nowTime = DateTime.Now;
			diff = nowTime - initialTime;
		} while (diff.Milliseconds < timeout);

		if (fail != null)
			fail ();
		yield return null;
	}

	public void SplitMessage () {
		string[] messageParts = incMessage.Split (new string[] {","}, StringSplitOptions.None);
		tempMessage = incMessage;
		if (messageParts [0].Trim ().IndexOf ('n') >= 0) {
			Debug.Log ("Received beat");
			spawnScript = spawner3.GetComponent<SpawnNew> (); 
			spawnScript.SpawnBeat ();
		} 
		else if (messageParts [0].Trim ().IndexOf ('u') >= 0) {
			Debug.Log ("Received undo");
		}
		else {
			Debug.Log ("is not a beat or undo");
			note = int.Parse (messageParts [0]);
			waveform = int.Parse (messageParts [1]);
			channel = int.Parse (messageParts [2]);
			chord = int.Parse (messageParts [3]);
		}
	}

	public void SpawnPlanet () {

		switch (channel) {
		case 0:
			spawnScript = spawner0.GetComponent<SpawnNew> (); 
			spawnScript.SpawnPlanet (note);
			break;
		case 1: 
			spawnScript = spawner1.GetComponent<SpawnNew> (); 
			spawnScript.SpawnPlanet (note);
			break;
		case 2: 
			spawnScript = spawner2.GetComponent<SpawnNew> (); 
			spawnScript.SpawnPlanet (note);
			break;
		}

		/*switch (waveform) {
		case 0: 
			spawnedWaveformer = Instantiate (squareWave, new Vector3 (-4.1f, 0.0f, 0.0f), Quaternion.Euler (0, -90, 0)) as GameObject;
			spawnedWaveformer.transform.SetParent (spawnedPlanet.transform, false);
			break;
		case 1:
			spawnedWaveformer = Instantiate (triangleWave, new Vector3(-4.1f,0.0f,0.0f), Quaternion.Euler(0,-90,0)) as GameObject;
			spawnedWaveformer.transform.SetParent (spawnedPlanet.transform, false);
			break;
		}*/
	}
}
