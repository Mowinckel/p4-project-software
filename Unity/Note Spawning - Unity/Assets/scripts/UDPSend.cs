﻿using UnityEngine;
using System.Collections;

using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

public class UDPSend : MonoBehaviour {

	private static int localPort;

	private string IP;
	public int port;

	IPEndPoint remoteEndPoint;
	UdpClient  client;

	private GameManager gm;

	private static void Main() {
		UDPSend sendObj = new UDPSend ();
		sendObj.init ();
	}

	// Use this for initialization
	void Start () {
		gm = GetComponent<GameManager> ();
		init ();
	}

	// Update is called once per frame
	void Update () {
	}

	void OnGUI() {
		if (gm.spawning && gm.tempMessage != "") {
			Debug.Log ("Spawning: " + gm.spawning);
			sendString ();
		}
	}

	public void init() {
		print ("UDPSend.init()");

		IP = "127.0.0.1";
		port = 12001;

		remoteEndPoint = new IPEndPoint (IPAddress.Parse (IP), port);
		client = new UdpClient ();

		print ("Sending to: " + IP + " : " + port);
		print ("Testing: nc -lu " + IP + " : " + port);
	}

	private void sendString() {
		try {
			byte[] data = Encoding.UTF8.GetBytes(gm.tempMessage);
			print("IncMessage: " + gm.tempMessage);
			client.Send(data, data.Length, remoteEndPoint);
		} catch (Exception err) {
			print (err.ToString ());
		}
	}

	private void sendEndless () {
		do {
			sendString ();
		} while (true);
	}
}
