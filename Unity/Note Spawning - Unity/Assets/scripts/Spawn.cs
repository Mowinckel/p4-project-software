﻿using UnityEngine;
using System.Collections;

public class Spawn : MonoBehaviour {

    //Used to identify different spawning points aka rings
    public int id = 0;
    //what ring the user has selected
    private int idSelector = 0;


    //Experience is a basic property
    public int IdSelector
    {
        get
        {
            //Some other code
            return idSelector;
        }
        set
        {
            //Some other code
            idSelector = value;
        }
    }

    public Transform Target;
    public GameObject NoteG;
    public GameObject NoteA;
    public GameObject NoteD;
    public GameObject NoteC;

    // Use this for initialization
    void Start () {
        //Moon = GameObject.Find("moon");

    }
	
	// Update is called once per frame
	void Update () {

        //selecting rings using up and down arrow keys
        if (Input.GetKeyDown(KeyCode.UpArrow))
            ++idSelector;
        
        if (Input.GetKeyDown(KeyCode.DownArrow))
            --idSelector;

        if (idSelector < 0)
            idSelector = 3;
        if (idSelector > 3)
            idSelector = 0;

        if(idSelector==0 && id == 0) {
            
            if (Input.GetKeyDown(KeyCode.G))
                
                Instantiate(NoteG, Target.position, Target.rotation);
                
            if(Input.GetKeyDown(KeyCode.A))
            Instantiate(NoteA, Target.position, Target.rotation);

            if (Input.GetKeyDown(KeyCode.D))
                Instantiate(NoteD, Target.position, Target.rotation);

            if (Input.GetKeyDown(KeyCode.C))
                Instantiate(NoteC, Target.position, Target.rotation);
        }

        if (idSelector == 1 && id == 1)
        {

            if (Input.GetKeyDown(KeyCode.G))

                Instantiate(NoteG, Target.position, Target.rotation);

            if (Input.GetKeyDown(KeyCode.A))
                Instantiate(NoteA, Target.position, Target.rotation);

            if (Input.GetKeyDown(KeyCode.D))
                Instantiate(NoteD, Target.position, Target.rotation);

            if (Input.GetKeyDown(KeyCode.C))
                Instantiate(NoteC, Target.position, Target.rotation);
        }

        if (idSelector == 2 && id == 2)
        {
            if (Input.GetKeyDown(KeyCode.G))

                Instantiate(NoteG, Target.position, Target.rotation);

            if (Input.GetKeyDown(KeyCode.A))
                Instantiate(NoteA, Target.position, Target.rotation);

            if (Input.GetKeyDown(KeyCode.D))
                Instantiate(NoteD, Target.position, Target.rotation);

            if (Input.GetKeyDown(KeyCode.C))
                Instantiate(NoteC, Target.position, Target.rotation);
        }

        if (idSelector == 3 && id == 3)
        {

            if (Input.GetKeyDown(KeyCode.G))

                Instantiate(NoteG, Target.position, Target.rotation);

            if (Input.GetKeyDown(KeyCode.A))
                Instantiate(NoteA, Target.position, Target.rotation);

            if (Input.GetKeyDown(KeyCode.D))
                Instantiate(NoteD, Target.position, Target.rotation);

            if (Input.GetKeyDown(KeyCode.C))
                Instantiate(NoteC, Target.position, Target.rotation);
        }

        //if (idSelector == 1 && Input.GetKeyDown(KeyCode.A) && id==1)
        //Instantiate(Note, Target.position, Target.rotation);

        //if (idSelector == 2 && Input.GetKeyDown(KeyCode.D) && id==2)
        //Instantiate(Note, Target.position, Target.rotation);

        //if (idSelector == 3 && Input.GetKeyDown(KeyCode.C) && id==3)
        //Instantiate(Note, Target.position, Target.rotation);
    }

	public void SpawnNote (int note, int waveform) {

		//selecting rings using up and down arrow keys
		if (Input.GetKeyDown(KeyCode.UpArrow))
			++idSelector;

		if (Input.GetKeyDown(KeyCode.DownArrow))
			--idSelector;

		if (idSelector < 0)
			idSelector = 3;
		if (idSelector > 3)
			idSelector = 0;

		if(idSelector==0 && id == 0) {

			if (Input.GetKeyDown(KeyCode.G))

				Instantiate(NoteG, Target.position, Target.rotation);

			if(Input.GetKeyDown(KeyCode.A))
				Instantiate(NoteA, Target.position, Target.rotation);

			if (Input.GetKeyDown(KeyCode.D))
				Instantiate(NoteD, Target.position, Target.rotation);

			if (Input.GetKeyDown(KeyCode.C))
				Instantiate(NoteC, Target.position, Target.rotation);
		}

		if (idSelector == 1 && id == 1)
		{

			if (Input.GetKeyDown(KeyCode.G))

				Instantiate(NoteG, Target.position, Target.rotation);

			if (Input.GetKeyDown(KeyCode.A))
				Instantiate(NoteA, Target.position, Target.rotation);

			if (Input.GetKeyDown(KeyCode.D))
				Instantiate(NoteD, Target.position, Target.rotation);

			if (Input.GetKeyDown(KeyCode.C))
				Instantiate(NoteC, Target.position, Target.rotation);
		}

		if (idSelector == 2 && id == 2)
		{
			if (Input.GetKeyDown(KeyCode.G))

				Instantiate(NoteG, Target.position, Target.rotation);

			if (Input.GetKeyDown(KeyCode.A))
				Instantiate(NoteA, Target.position, Target.rotation);

			if (Input.GetKeyDown(KeyCode.D))
				Instantiate(NoteD, Target.position, Target.rotation);

			if (Input.GetKeyDown(KeyCode.C))
				Instantiate(NoteC, Target.position, Target.rotation);
		}

		if (idSelector == 3 && id == 3)
		{

			if (Input.GetKeyDown(KeyCode.G))

				Instantiate(NoteG, Target.position, Target.rotation);

			if (Input.GetKeyDown(KeyCode.A))
				Instantiate(NoteA, Target.position, Target.rotation);

			if (Input.GetKeyDown(KeyCode.D))
				Instantiate(NoteD, Target.position, Target.rotation);

			if (Input.GetKeyDown(KeyCode.C))
				Instantiate(NoteC, Target.position, Target.rotation);
		}
	}
}
