﻿using UnityEngine;
using System.Collections;

public class Contact : MonoBehaviour
{


    public int position = 1;

    public void OnTriggerEnter2D(Collider2D other)
    {

        if(other.tag == "Snappoint3")
            {
			StartCoroutine(Offset ());
            }
    }

	IEnumerator Offset() {
		yield return new WaitForSeconds (0.25f);
			--position;
		if (position < 0)
			position = 7;
    }
}
