﻿using UnityEngine;
using System.Collections;

class Planet {

    private int ring;
    private int pos;

    public Planet (int tempRing, int tempPos)
    {
        ring = tempRing;
        pos = tempPos;

    }

    public int GetRing()
    {
        return ring;
    }
    public int GetPos()
    {
        return pos;
    }
}
