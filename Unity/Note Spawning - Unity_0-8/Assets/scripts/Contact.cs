﻿using UnityEngine;
using System.Collections;

public class Contact : MonoBehaviour {


	public int position = 0;


    public void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Snappoint3")
            {
                --position;
            if (position < 0)
                position = 7;
            }
    }

}
