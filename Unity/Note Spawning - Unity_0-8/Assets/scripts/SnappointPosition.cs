﻿using UnityEngine;
using System.Collections;

public class SnappointPosition : MonoBehaviour {

	public GameObject contactPrefab;

    private SpawnNew spawnPos;
    private Contact contactCol;
	public int ring = 0;
	public int pos = 0;
	public Vector3 currentSnap;
    public Vector3 beatSnap;
    
	public void Start () {
		spawnPos = gameObject.GetComponent<SpawnNew> (); // consider if we need spawnNew or jsut Sapwn
		contactCol = contactPrefab.GetComponent<Contact> ();
        Debug.Log("THIS IS THE CURRENT POSITION OF THE SNAPPING POINTS RAWR RAWR RAWR");
    }

	public void Update ()
	{
		ring = spawnPos.idSelector;
		pos = contactCol.position;

		currentSnap = GameObject.Find ("Snappoint " + ring + "." + pos).transform.position;
        //Debug.Log("THIS IS THE CURRENT POSITION OF THE SNAPPING POINTS RAWR RAWR RAWR");
        beatSnap = GameObject.Find("Snappoint 3." + pos).transform.position;
	}
}
