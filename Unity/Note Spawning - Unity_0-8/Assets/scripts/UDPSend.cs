﻿using UnityEngine;
using System.Collections;

using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

public class UDPSend : MonoBehaviour {

	private static int localPort;

	private string IP;
	public int port;

	IPEndPoint remoteEndPoint;
	UdpClient  client;

	private GameManager gm;

	string strMessage = "";

	private static void Main() {
		UDPSend sendObj = new UDPSend ();
		sendObj.init ();

		sendObj.sendEndless ();
	}

	// Use this for initialization
	void Start () {
		gm = GetComponent<GameManager> ();
		init ();
	}

	// Update is called once per frame
	void Update () {
	}

	void OnGUI() {
		strMessage = "Hello World";
		if (gm.spawning && gm.incMessage != "") {
			Debug.Log ("Spawning: " + gm.spawning);
			sendString ();
		}
	}

	public void init() {
		print ("UDPSend.init()");

		IP = "127.0.0.1";
		port = 12001;

		remoteEndPoint = new IPEndPoint (IPAddress.Parse (IP), port);
		client = new UdpClient ();

		print ("Sending to: " + IP + " : " + port);
		print ("Testing: nc -lu " + IP + " : " + port);
	}

	private void inputFromConsole() {
		try {
			string text;
			do {
				text = Console.ReadLine();

				if (text != "") {
					byte[] data = Encoding.UTF8.GetBytes(text);
					client.Send(data, data.Length, remoteEndPoint);
				}
			} while (text != "");
		} catch (Exception err) {
			print (err.ToString ());
		}
	}

	private void sendString() {
		try {
			byte[] data = Encoding.UTF8.GetBytes(gm.incMessage);
			print("IncMessage: " + gm.incMessage);
			client.Send(data, data.Length, remoteEndPoint);
		} catch (Exception err) {
			print (err.ToString ());
		}
	}

	private void sendEndless () {
		do {
			sendString ();
		} while (true);
	}
}
