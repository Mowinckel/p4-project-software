﻿using UnityEngine;
using System.Collections;

public class SpawnNew : MonoBehaviour {

    //Used to identify different spawning points aka rings
    public int id = 0;
    //what ring the user has selected
    public int idSelector = 1;
	public Vector3 current;
    public Vector3 beatCurrent;
    public Transform Target;
    public GameObject NoteG;
    public GameObject NoteA;
    public GameObject NoteD;
    public GameObject NoteC;
    public GameObject Beat;
    private GameObject[,] PlanetArray;
    private int newRing;
    private int newPos;

	public SnappointPosition snapPosition;

    // Use this for initialization
    void Start () {
        //Moon = GameObject.Find("moon");
		snapPosition = gameObject.GetComponent<SnappointPosition> ();

        PlanetArray = new GameObject[32, 32];



    }
	
	// Update is called once per frame
	void Update () {
         Orbit orbitspeed = GetComponent<Orbit>();
		current = snapPosition.currentSnap;
        beatCurrent = snapPosition.beatSnap;
        newRing = snapPosition.ring;
        newPos = snapPosition.pos;
        

        //selecting rings using up and down arrow keys
        if (Input.GetKeyDown(KeyCode.UpArrow))
            ++idSelector;
        
        if (Input.GetKeyDown(KeyCode.DownArrow))
            --idSelector;

        if (idSelector < 0)
            idSelector = 2;
        if (idSelector > 2)
            idSelector = 0;
            
            if (Input.GetKeyDown(KeyCode.B))
                
				Instantiate(Beat, beatCurrent, Target.rotation);
                

        if (idSelector == 1 && id == 1)
        {
            //orbitspeed.speed = 60;

            if (Input.GetKeyDown(KeyCode.G))

				Instantiate(NoteG, current, Target.rotation);

            if (Input.GetKeyDown(KeyCode.A))
				Instantiate(NoteA, current, Target.rotation);

            if (Input.GetKeyDown(KeyCode.D))
				Instantiate(NoteD, current, Target.rotation);

            if (Input.GetKeyDown(KeyCode.C))
				Instantiate(NoteC, current, Target.rotation);
        }

        if (idSelector == 2 && id == 2)
        {
            //orbitspeed.speed = 60;

            if (Input.GetKeyDown(KeyCode.G))

				Instantiate(NoteG, current, Target.rotation);

            if (Input.GetKeyDown(KeyCode.A))
				Instantiate(NoteA, current, Target.rotation);

            if (Input.GetKeyDown(KeyCode.D))
				Instantiate(NoteD, current, Target.rotation);

            if (Input.GetKeyDown(KeyCode.C))
				Instantiate(NoteC, current, Target.rotation);
        }

        if (idSelector == 0 && id == 0)
        {
            //orbitspeed.speed = 60;

            if (Input.GetKeyDown(KeyCode.G))

                Instantiate(NoteG, current, Target.rotation);

            if (Input.GetKeyDown(KeyCode.A))
                Instantiate(NoteA, current, Target.rotation);

            if (Input.GetKeyDown(KeyCode.D))
                Instantiate(NoteD, current, Target.rotation);

            if (Input.GetKeyDown(KeyCode.C)) { 
            Instantiate(NoteC, current, Target.rotation);
            
            
        }
                
        }
    }
}
