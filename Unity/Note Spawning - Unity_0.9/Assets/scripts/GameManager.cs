﻿using UnityEngine;
using System.Collections;
using System.IO.Ports;
using System;

public class GameManager : MonoBehaviour {

	SerialPort stream = new SerialPort("COM4", 9600);
	string incMessage;
	public string tempMessage;
	public bool spawning = false;
	int note = -1;
	public int waveform;
	public int channel;
	public int chord;
	ParticleSystemRenderer psr;
	GameObject spawnedPlanet;
	GameObject spawnedWaveformer;

	public GameObject squareWave;
	public GameObject triangleWave;

	public GameObject spawner0;
	public GameObject spawner1;
	public GameObject spawner2;
	public GameObject spawner3;

	public Transform spawnPoint;

	private  SpawnNew spawnScript;
	public static bool started = false;



	// Use this for initialization
	void Start () {
		stream.ReadTimeout = 50;
		stream.Open ();
	}

	// Update is called once per frame
	void Update () {
		StartCoroutine (
			AsynchronousReadFromArduino (
				(string s) => Debug.Log (s),
				() => Debug.Log (""),
				0.5f
			)
		);
	}

	public IEnumerator AsynchronousReadFromArduino(Action<string> callback, Action fail = null, float timeout = float.PositiveInfinity) {
		DateTime initialTime = DateTime.Now;
		DateTime nowTime;
		TimeSpan diff = default(TimeSpan);

		string dataString = null;

		do {
			try {
				dataString = stream.ReadLine ();
			} catch (TimeoutException) {
				dataString = null;
			}

			if (dataString != null) {
				started = true;
				incMessage = dataString;
				Debug.Log("Message from Arduino: " + incMessage);
				callback (dataString);
				SplitMessage();
				spawning = true;
				yield return null;
			} else
				spawning = false;
			yield return new WaitForSeconds (0.05f);

			nowTime = DateTime.Now;
			diff = nowTime - initialTime;
		} while (diff.Milliseconds < timeout);

		if (fail != null)
			fail ();
		yield return null;
	}


	public void SplitMessage () {
		string[] messageParts = incMessage.Split (new string[] {","}, StringSplitOptions.None);
		tempMessage = incMessage;
		Debug.Log(incMessage.IndexOf("1"));
		if (incMessage.Length > 20) {
			spawner0.GetComponent<SpawnNew> ().UniquePlanetCounter ();
			ForwardDetection[] planetsCorrectionList = FindObjectsOfType (typeof(ForwardDetection)) as ForwardDetection[];
			foreach (ForwardDetection planetToBeCorrected in planetsCorrectionList) {
				planetToBeCorrected.DetectCorrect ();
			}
		} else {
			if (messageParts [0].Trim ().IndexOf ('n') >= 0 && incMessage.IndexOf ("1") > 0) {
				spawnScript = spawner3.GetComponent<SpawnNew> (); 
				spawnScript.SpawnBeat ();
				spawner0.GetComponent<SpawnNew> ().UniquePlanetCounter ();
			} else if (messageParts [0].Trim ().IndexOf ('u') >= 0) {
				Debug.Log ("Undo received");
				switch (channel) { 
				case 0:
					spawnScript = spawner0.GetComponent<SpawnNew> (); 
					spawnScript.Undo ();
					break;
				case 1: 
					spawnScript = spawner1.GetComponent<SpawnNew> (); 
					spawnScript.Undo ();
					break;
				case 2: 
					spawnScript = spawner2.GetComponent<SpawnNew> (); 
					spawnScript.Undo ();
					break;
				case 3: 
					spawnScript = spawner3.GetComponent<SpawnNew> (); 
					spawnScript.Undo ();
					break;
				}
			}
			else if (incMessage.IndexOf ("1") < 0) {
				Debug.Log ("Invalid message sent from Arduino: " + incMessage);
			}
			 else {
				note = int.Parse (messageParts [0]);
				waveform = int.Parse (messageParts [1]);
				channel = int.Parse (messageParts [2]);
				chord = int.Parse (messageParts [3]);
				if (note != -1) {
					SpawnPlanet();
				}
			}
		}
	}

	public void SpawnPlanet () {

		switch (channel) {
		case 0:
			spawnScript = spawner0.GetComponent<SpawnNew> (); 
			spawnScript.SpawnPlanet (note);
			break;
		case 1: 
			spawnScript = spawner1.GetComponent<SpawnNew> (); 
			spawnScript.SpawnPlanet (note);
			break;
		case 2: 
			spawnScript = spawner2.GetComponent<SpawnNew> (); 
			spawnScript.SpawnPlanet (note);
			break;
		}
	}
}
