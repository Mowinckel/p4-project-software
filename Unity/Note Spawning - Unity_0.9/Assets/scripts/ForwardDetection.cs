﻿using UnityEngine;
using System.Collections;

public class ForwardDetection : MonoBehaviour {
    private GameObject previousPlanet;
    private Transform tempMoon;
    private Transform previousMoon;
    private Transform tempWave;
    private Transform previousWave;
    private int newRing;
    private int newPos;
    private int previousPos;


    // Use this for initialization
    void Start () {
    }



// Update is called once per frame
	void Update()
    {
      
    }
    
	public void DetectCorrect () {
		string name = this.gameObject.name;
		char i = name[7];
		char e = name[8];
		try
		{
			newRing = int.Parse(e.ToString());
			newPos = int.Parse(i.ToString());
			previousPos = newPos + 1;
		}
		catch
		{
			Debug.Log("Forward Detection failed");
		}
		if (previousPos > 7)
			previousPos = 0;
		try
		{
			previousPlanet = GameObject.Find("Planet " + previousPos + newRing);
		}
		catch
		{
			previousPlanet = null;
		}
		if (previousPlanet != null)
		{
			tempMoon = this.gameObject.transform.Find("moon");
			previousMoon = previousPlanet.transform.Find("moon");
			tempWave = this.gameObject.transform.Find("waveform");
			previousWave = previousPlanet.transform.Find("waveform");
			if (previousPos > 7)
				previousPos = 0;


			if (tempMoon.tag == previousMoon.tag && tempWave.tag == previousWave.tag && previousPlanet.name == ("Planet " + previousPos + newRing))
			{
				Renderer ren = tempMoon.GetComponent<Renderer>();
				ren.enabled = false;
				SpriteRenderer sren = tempMoon.GetComponent<SpriteRenderer>();
				sren.enabled = false;
			}
			else if (this.name == ("Planet " + newPos + newRing))
			{
				Renderer ren = tempMoon.GetComponent<Renderer>();
				ren.enabled = true;
				SpriteRenderer sren = tempMoon.GetComponent<SpriteRenderer>();
				sren.enabled = true;
			}

		}
	}
}