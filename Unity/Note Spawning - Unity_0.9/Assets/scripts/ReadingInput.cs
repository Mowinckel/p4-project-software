﻿using UnityEngine;
using System.Collections;
using System.IO.Ports;
using System;

public class ReadingInput : MonoBehaviour {

	SerialPort stream = new SerialPort("COM3", 9600);

	// Use this for initialization
	void Start () {
		stream.ReadTimeout = 50;
		stream.Open ();
	}

	// Update is called once per frame
	void Update () {
		StartCoroutine (
			AsynchronousReadFromArduino (
				(string s) => Debug.Log (s),
				() => Debug.LogError ("Error!"),
				0.5f
			)
		);
		string value = stream.ReadLine ();
		Debug.Log (value);
		stream.BaseStream.Flush ();
		

	}

	public IEnumerator AsynchronousReadFromArduino(Action<string> callback, Action fail = null, float timeout = float.PositiveInfinity) {
		DateTime initialTime = DateTime.Now;
		DateTime nowTime;
		TimeSpan diff = default(TimeSpan);

		string dataString = null;

		do {
			try {
				dataString = stream.ReadLine ();
			} catch (TimeoutException) {
				dataString = null;
			}

			if (dataString != null) {
				callback (dataString);
				yield return null;
			} else
				yield return new WaitForSeconds (0.05f);

			nowTime = DateTime.Now;
			diff = nowTime - initialTime;
		} while (diff.Milliseconds < timeout);

		if (fail != null)
			fail ();
		yield return null;
	}
}
