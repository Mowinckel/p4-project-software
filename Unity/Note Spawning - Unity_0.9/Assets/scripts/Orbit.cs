﻿using UnityEngine;
using System.Collections;

public class Orbit : MonoBehaviour {

	public Transform target;
	public float rotateSpeed = 240.0f;
	private ParticleSystem ps;


	// Use this for initialization
	void Start () {
		ps = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate (0,0,rotateSpeed*Time.deltaTime); //rotates rotateSpeed degrees per second around z axis
	}

	void OnTriggerEnter2D (Collider2D col) {
		if (col.tag == "Start") {
			ps.Play();
		}
	}
}
