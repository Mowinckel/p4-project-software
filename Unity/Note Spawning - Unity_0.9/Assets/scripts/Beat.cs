﻿using UnityEngine;
using System.Collections;

public class Beat : MonoBehaviour {

	public GameObject particlePrefab;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D (Collider2D col) {
//		Debug.Log(col.name);
		if (col.tag == "Start") {
			Instantiate (particlePrefab, transform.position, Quaternion.identity);
		}
	}
}
