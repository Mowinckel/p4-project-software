﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class SpawnNew : MonoBehaviour {

    //Used to identify different spawning points aka rings
    public int id = 0;
    //what ring the user has selected
    public int idSelector = 1;
	public Vector3 current;
    public Vector3 beatCurrent;
    public Transform Target;
	public GameObject NoteG;
	public GameObject NoteG1;
	public GameObject NoteG2;
	public GameObject NoteA;
	public GameObject NoteA1;
	public GameObject NoteA2;
	public GameObject NoteD;
	public GameObject NoteD1;
	public GameObject NoteD2;
	public GameObject NoteC;
	public GameObject NoteC1;
	public GameObject NoteC2;
	public GameObject NoteE;
	public GameObject NoteE1;
	public GameObject NoteE2;
    public GameObject Beat;
    public GameObject oldPlanetClone = null;
	public GameObject gm;

	GameObject tempPlanet;
	GameObject previousPlanet;
	public GameObject squareWaveformer;
	public GameObject pulseWaveformer;
	public GameObject sawtoothWaveformer;
    public GameObject triangleWaveformer;
	private Transform tempMoon;
	private Transform previousMoon;
	private Transform tempWave;
	private Transform previousWave;

	private Transform moving;
	private float lifetime;
	private GameObject waveSelector;

    public SnappointPosition snapPosition;
	static List <Planet> planetList = new List<Planet> ();

	int planetCount = 0;
	int pastPlanetCount = 0;

	GameObject[] clones;

    // Use this for initialization
    void Start () {
		snapPosition = gameObject.GetComponent<SnappointPosition> ();
	}
	
	// Update is called once per frame
	void Update () {
		idSelector = gm.GetComponent<GameManager> ().channel;
		current = snapPosition.currentSnap;
		beatCurrent = snapPosition.beatSnap;
		if (snapPosition.pos == 0) {
			ForwardDetection[] planetsCorrectionList = FindObjectsOfType (typeof(ForwardDetection)) as ForwardDetection[];
			foreach (ForwardDetection planetToBeCorrected in planetsCorrectionList) {
				planetToBeCorrected.DetectCorrect ();
			}
		}
    }

	void addToArrayList (GameObject note)
	{
		GameObject newNote = note;
        GameObject waveInstance;
        Planet planet = new Planet(snapPosition.ring, snapPosition.pos);
		planet.setID (planetCount);
        int newPos = planet.GetPos();
        int newRing = planet.GetRing();
        int length = planetList.Count;

        for (int i = 0; i < length; i++)
        {
            int tempPos = planetList[i].GetPos();
            int tempRing = planetList[i].GetRing();
            GameObject oldPlanet = GameObject.Find("Planet " + tempPos + tempRing);
            if (length != 0)
            {
                if (tempPos == newPos && tempRing == newRing)
                {
					moving = oldPlanet.transform;
					Vector3 movingSnap = moving.transform.position;
					oldPlanetClone = Instantiate (oldPlanet, movingSnap, Target.rotation) as GameObject;
					oldPlanetClone.tag = "Clone";

					foreach (Renderer r in oldPlanetClone.GetComponentsInChildren<Renderer>())
						r.enabled = false;
                    Destroy (oldPlanet);
                    break;
                }
            }
        }
        planetList.Add(planet);
        tempPlanet = (GameObject)Instantiate (newNote, current, Target.rotation);
		pastPlanetCount = planetCount;
        tempPlanet.name = "Planet " + newPos + newRing;
		setWaveform ();
        waveInstance = Instantiate(waveSelector, new Vector3(0f, 0.0f, 0.0f), Quaternion.Euler(0, -90, 0)) as GameObject;
        waveInstance.transform.SetParent(tempPlanet.transform, false);
        waveInstance.name = "waveform";
        int previousPos = newPos - 1;
        if (previousPos < 0)
            previousPos = 7;

		if (planet.getID() > pastPlanetCount && GameObject.FindGameObjectsWithTag("Clone").Length != 0) {
			for (int i = 0; i < clones.Length; i++) {
				Destroy (clones [i]);
			}
		}

    }


	public void SpawnBeat () {
		Debug.Log ("Beat spawned at: " + snapPosition.pos);
		beatCurrent = snapPosition.beatSnap;
		GameObject tempBeat;
		tempBeat = Instantiate(Beat, beatCurrent, Target.rotation) as GameObject;
		tempBeat.name = "Beat " + snapPosition.pos;
		Planet planet = new Planet (3, snapPosition.pos);
		planet.setID (planetCount);
		planetList.Add(planet);
		Debug.Log ("PlanetCount: " + planetCount);
		UniquePlanetCounter ();
	}

	public void SpawnPlanet (int note) {
		current = snapPosition.currentSnap;
		beatCurrent = snapPosition.beatSnap;

		//selecting rings using up and down arrow keys

		if (idSelector == 0 && id == 0) {
			AddFromNote (note);
		}

		if (idSelector == 1 && id == 1) {
			AddFromNote (note);
		}

		if (idSelector == 2 && id == 2) {
			AddFromNote (note);
		}


	}

	void AddFromNote (int note) {
		switch (note) {
		case 0: 
			if (gm.GetComponent<GameManager> ().chord == 0)
				addToArrayList (NoteC);
			if (gm.GetComponent<GameManager> ().chord == 1)
				addToArrayList (NoteC1);
			if (gm.GetComponent<GameManager> ().chord == 2)
				addToArrayList (NoteC2);
			break;
		case 1:
			if (gm.GetComponent<GameManager> ().chord == 0)
				addToArrayList (NoteD);
			if (gm.GetComponent<GameManager> ().chord == 1)
				addToArrayList (NoteD1);
			if (gm.GetComponent<GameManager> ().chord == 2)
				addToArrayList (NoteD2);
			break;
		case 2: 
			if (gm.GetComponent<GameManager> ().chord == 0)
				addToArrayList (NoteG);
			if (gm.GetComponent<GameManager> ().chord == 1)
				addToArrayList (NoteG1);
			if (gm.GetComponent<GameManager> ().chord == 2)
				addToArrayList (NoteG2);
			break;
		case 3:
			if (gm.GetComponent<GameManager> ().chord == 0)
				addToArrayList (NoteA);
			if (gm.GetComponent<GameManager> ().chord == 1)
				addToArrayList (NoteA1);
			if (gm.GetComponent<GameManager> ().chord == 2)
				addToArrayList (NoteA2);
			break;
		case 4:
			if (gm.GetComponent<GameManager> ().chord == 0)
				addToArrayList (NoteE);
			if (gm.GetComponent<GameManager> ().chord == 1)
				addToArrayList (NoteE1);
			if (gm.GetComponent<GameManager> ().chord == 2)
				addToArrayList (NoteE2);
			break;
		}
	}

	public void Undo() {
		Debug.Log ("Planets in planetList: " + planetList.Count);
		/*Planet toDelete = planetList[planetCount - 1];
		GameObject deleteP = GameObject.Find ("Planet " + toDelete.GetPos () + toDelete.GetRing ());
		GameObject deleteB = GameObject.Find ("Beat " + toDelete.GetPos());
		Destroy (deleteP);
		Destroy (deleteB);*/
		for (int i = 0; i < planetList.Count; i++) {
			Debug.Log ("Planetcount: " + planetCount + " PlanetID: " + planetList[i].getID ());
			if (planetList[i].getID() == planetCount-1) {
				Debug.Log ("Goes inside undo condition");
				GameObject deletePlanet = GameObject.Find ("Planet " + planetList[i].GetPos () + planetList[i].GetRing ());
				GameObject deleteBeat = GameObject.Find ("Beat " + planetList[i].GetPos());
				Destroy (deletePlanet);
				Destroy (deleteBeat);
			}
		}
		planetList.Clear ();

		Renderer[] renderers = FindObjectsOfType (typeof(Renderer)) as Renderer[];
		SpriteRenderer[] srenderers = FindObjectsOfType (typeof(SpriteRenderer)) as SpriteRenderer[];

		foreach (Renderer ren in renderers) {
			ren.enabled = true;
		}
		foreach (SpriteRenderer sren in srenderers) {
			sren.enabled = true;
		}

		clones = GameObject.FindGameObjectsWithTag ("Clone");
		for (int i = 0; i < clones.Length; i++) {
			foreach (Renderer ren in clones[i].GetComponentsInChildren<Renderer>()) {
				ren.enabled = true;
			}
			foreach (SpriteRenderer sren in clones[i].GetComponentsInChildren<SpriteRenderer>()) {
				sren.enabled = true;
			}
			clones [i].tag = "Untagged";
			string cloneName = clones [i].name;
			char cPos = cloneName[7];
			char cRing = cloneName[8];
			int iPos = int.Parse(cPos.ToString());
			int iRing = int.Parse (cRing.ToString());
			clones [i].name = "Planet " + iPos + iRing;
		}
		ForwardDetection[] planetsCorrectionList = FindObjectsOfType (typeof(ForwardDetection)) as ForwardDetection[];
		foreach (ForwardDetection planetToBeCorrected in planetsCorrectionList) {
			planetToBeCorrected.DetectCorrect ();
		}

	}

	void setWaveform () {
		int waveform = gm.GetComponent<GameManager>().waveform;
		switch (waveform) {
		case 0: 
			waveSelector = squareWaveformer;
			break;
		case 1:
			waveSelector = triangleWaveformer;
			break;
		case 2:
			waveSelector = sawtoothWaveformer;
			break;
		case 3:
			waveSelector = pulseWaveformer;
			break;
		}
	}

	public void UniquePlanetCounter() {
		planetCount++;
	}
}

class Planet
{
    private int ring;
    private int pos;
	private int id;

    public Planet(int tempRing, int tempPos)
    {
        ring = tempRing;
        pos = tempPos;
    }

    public int GetRing()
    {
        return ring;
    }
    public int GetPos()
    {
        return pos;
    }
	public int getID() {
		return id;
	}
	public void setID(int newID) {
		id = newID;
	}
		
}
