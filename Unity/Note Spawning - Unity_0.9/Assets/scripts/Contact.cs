﻿using UnityEngine;
using System.Collections;

public class Contact : MonoBehaviour
{
	public int position;

	void Start () {
		position = 0;
	}

    public void OnTriggerEnter2D(Collider2D other)
    {
		if(other.tag == "Snappoint3" && GameManager.started)
            {
			char c = other.name [12];
			string s = c.ToString ();
			int i = int.Parse (s);
			StartCoroutine (Offset (i));
            } 
    }

	IEnumerator Offset(int i) {
		position = i;
		yield return new WaitForSeconds (0.15f);
		position--;
		if (position < 0)
			position = 7;
    }
}
