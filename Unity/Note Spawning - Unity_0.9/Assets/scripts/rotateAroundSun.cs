﻿using UnityEngine;
using System.Collections;

public class rotateAroundSun : MonoBehaviour {

	private Transform target;
	private float speed = 90; // Degrees/second
	private Vector3 zAxis = new Vector3(0,0,-1);


	// Use this for initialization
	void Start () {
		target = GameObject.FindGameObjectWithTag ("Sun").GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (target != null && GameManager.started) {
			transform.RotateAround(target.position, zAxis, speed * Time.deltaTime);
		}
	}
}
