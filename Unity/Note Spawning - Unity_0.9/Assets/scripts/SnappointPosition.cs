﻿using UnityEngine;
using System.Collections;

public class SnappointPosition : MonoBehaviour {

	public GameObject contactPrefab;

    private SpawnNew spawnPos;
    private Contact contactCol;
	public int ring = 0;
	public int pos = 0;
	public Vector3 currentSnap;
    public Vector3 beatSnap;
    
	public void Start () {
		spawnPos = gameObject.GetComponent<SpawnNew> (); // consider if we need spawnNew or just Spawn
		contactCol = contactPrefab.GetComponent<Contact> ();
    }

	public void Update ()
	{
		ring = spawnPos.idSelector;
		pos = contactCol.position;

		currentSnap = GameObject.Find ("Snappoint " + ring + "." + pos).transform.position;
        beatSnap = GameObject.Find("Snappoint 3." + pos).transform.position;
	}
}
