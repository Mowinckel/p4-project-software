import processing.serial.*;
import ddf.minim.*;
import ddf.minim.ugens.*;
import java.util.ArrayList;
import java.util.Arrays;


//Global variables
//These variables are for serial communication

//commented out temporarily, for testing purposes
//boolean pressed = false; 
boolean pressed = true;

boolean[] frameToBoolean = new boolean[8];
// this is the list of input notes
ArrayList<Input> inputObject = new ArrayList<Input>();
Input tempInput = null;
int id;
int note;
int waveform;
int channel;

Serial myPort;

int counter;
int arrayPosition =0;

//maybe turn into array[4]
//ArrayList<ADSR> adsr = new ArrayList<ADSR>();
ADSR[] adsr = new ADSR[4];
ADSR adsrTemp;

//These are for minim audio
AudioOutput aOut;
Minim minim;

//Input objects and arrays
Input[][] soundLoop = new Input[8][4]; // [8] = 4 seconds [4] the 4 different channels
boolean[] dummyBoolArray = new boolean[] {false, false, false, false, false, false, false, false};
Input dummy = new Input(-1, 0, 0, 0, 0, dummyBoolArray);

//Oscillators
Oscil tempOsc = new Oscil(0,0,Waves.SINE);

//maybe change to arraylist
Oscil[] oscillators = new Oscil[4];
//General variables used in the program
boolean alreadyPlayed = false;

//Testing
boolean bla = true;
String val =     "0, 3, 2, 3, false, false, true, false, false, false, false, false, ";  //note, waveform, channel, chord, rest are boolean values
String valTest = "1, 2, 2, 7, false, false, false, true, false, false, false, false, ";


void setup() {
  minim = new Minim(this);
  
  /*
  //commented out for testing purposes
  printArray(Serial.list()); //Writes the port(s) in use 
  String portName = Serial.list()[0];
  myPort = new Serial(this, portName, 9600);
  myPort.bufferUntil('\n');
  */
  aOut = minim.getLineOut();
  for (int i = 0; i < adsr.length; ++i)
    adsr[i] = new ADSR(0.0, 0.0, 0.0, 0.0, 0.0);
 
  //frameRate(2.5); //This equals to 150BPM
 // frameRate(0.25);
}

void draw() {
if (pressed) {
     splitMessage(val);
     overrideInputObjects();
     createSoundArray(inputObject);
     print2Darray();
    
    
     splitMessage(valTest);
     overrideInputObjects();
     createSoundArray(inputObject);
     print2Darray();
     
     pressed = false;
   }
   else {
     tempOsc.unpatch(aOut);
     alreadyPlayed = false;
   } 
   
   //why we need this function?  
   //frameNumber();    
  
   playSoundArray();
 
}

void print2Darray(){
   for (int i = 0; i < soundLoop.length; i++) {
       for (int j = 0; j < soundLoop[i].length; j++) {
         print(soundLoop[i][j] + " ");
       }
       println("");
     }
     println("");
}

void playTemporary () {
  if (!alreadyPlayed) {
    Input tempInput = new Input(note, waveform); //Use this for inpiration to adding objects to SoundLoop
    //Fill oscillator with note and wave information
    tempOsc = new Oscil(tempInput.getFrequency(), 1.0f, tempInput.getWaveform());
    //Play oscillator as long as that type of serial message is true
    tempOsc.patch(aOut);
    //Stop oscillator when that type is false
    alreadyPlayed = true;
  }
  
}

void createSoundArray (ArrayList<Input> inputs) {
  //Store values in a 2D array with x = channels, y = i (current frame)
  for (int i = 0; i < soundLoop.length; i++) {
     for (int j = 0; j < soundLoop[i].length; j++) {
       for (int k = 0; k < inputs.size(); k++) {
         if (j == inputs.get(k).getChannel() && inputs.get(k).getTime(i)) {
           soundLoop[i][j] = inputs.get(k);
         }
   
       }
       if (soundLoop[i][j] == null)
         soundLoop[i][j] = dummy;
     }
  }
}

//does not account for the edges of soundloop
//bug if notes played in one channel one after another
void playSoundArray () {
  for (int i = 0; i < soundLoop.length; i++) {
   for (int j = 0; j < soundLoop[i].length; j++) {
     int beforeI = i-1;
     if (beforeI < 0)  beforeI = 0;
     int afterI = i+1;
       if (afterI == soundLoop.length) {
         adsr[j].noteOff(); 
         afterI = 0;
           }
         
      if (soundLoop[beforeI][j].getTrueValues() == 1){
           println("works");
           adsr[j].noteOff();
       }
           
      if (soundLoop[i][j].getID() != -1 && (soundLoop[i][j].getID() != soundLoop[beforeI][j].getID())) {
           Oscil mainOsc = new Oscil(soundLoop[i][j].getFrequency(), 0.8f, soundLoop[i][j].getWaveform());
           adsr[j] = new ADSR(0.6, 0.04, 0.09, 0.1, 0.1);
           mainOsc.patch(adsr[j]);
           adsr[j].patch(aOut);
           adsr[j].noteOn();
           println("Playing " + soundLoop[i][j].getID());
       }
       
      else if (soundLoop[i][j].getID() != -1 && (soundLoop[i][j].getID() != soundLoop[afterI][j].getID())) {
         adsr[j].noteOff();
          println("Stopping " + soundLoop[i][j].getID());
       }
      
     }
     delay(250);
  }
}

/*
void playSoundArray () {
  for (int o = 0; o < oscillators.length; o++) {
       oscillators[o] = new Oscil(0, 0, Waves.SINE);
       
  }
  for (int i = 0; i < soundLoop.length; i++) {
   for (int j = 0; j < soundLoop[i].length; j++) {
     Input currentObject = soundLoop[i][j];  
     
       if (currentObject != null) {
           oscillators[j] = new Oscil(currentObject.getFrequency(), 0.8f, currentObject.getWaveform());
           //println(" Playing Object: " + currentObject.getID());
           adsrTemp = new ADSR(0.6, 0.04*8, 0.05*8, 0.141*8, 0.1*8);
           adsr.add(adsrTemp);
           oscillators[j].patch(adsrTemp);
           adsrTemp.patch(aOut);
           adsrTemp.noteOn();
       }
     }
    delay(150*8);
    for (int m = 0; m< adsr.size(); m++){
      adsr.get(m).noteOff();
      //adsr.remove(m);
    }
    delay(100*8);   
  }
}*/

void splitMessage(String message) {
  String[] parts = split(message,','); //(1,3,0)
  note = Integer.parseInt(parts[0].trim()); 
  waveform = Integer.parseInt(parts[1].trim());
  channel = Integer.parseInt(parts[2].trim());
  int chord = Integer.parseInt(parts[3].trim());
  frameToBoolean = new boolean[8];
  for (int i = 0; i < frameToBoolean.length; i++){
    frameToBoolean[i] = Boolean.parseBoolean(parts[i+4].trim());
    }
    tempInput = new Input(id, note, waveform, channel, chord, frameToBoolean);
    ++id;
    
 }
  

 void overrideInputObjects(){
   for( int i= 0; i< inputObject.size(); ++i){
      if (tempInput.getChannel() == inputObject.get(i).getChannel()){ 
        for( int j= 0; j<frameToBoolean.length; ++j){
         if (inputObject.get(i).getTime(j) && tempInput.getTime(j)){
            inputObject.get(i).setTimeToFalse(j); 
        }
        
       
      }
   }
   if (inputObject.get(i).getFalseValues()){
          inputObject.remove(inputObject.get(i));
      }
   }
   
     inputObject.add(tempInput);
     
     //make function remove those objects, that have only false values in them
     
 }
 

//commented out for testing purposes
/*
void serialEvent(Serial p) { 
  //Detects if serial communication is actives
  p = myPort;
  val = p.readStringUntil('\n');
  pressed = true;
}
*/

/*
void frameNumber(){
  ++counter;
  if (counter>=8)
  counter = 0;
  delay(500);
  
}*/