import hypermedia.net.*;

UDP udp;
int port = 12001;
String hostIP = "127.0.0.1";


void setup() {
  size(480,640);
  background(0);
  smooth();
  
  udp = new UDP(this, port, hostIP);
  udp.log(true);
  udp.listen(true);
  noLoop();
}

void draw () {
  
}



void receive(byte[] data, String ip, int port) {
  String message = new String(data);
  println("Received: " + message);
}