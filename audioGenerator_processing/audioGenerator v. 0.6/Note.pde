class Input {
  private int id;
  private int frequency;
  private int waveform;
  private int channel;
  private int chord;
  private boolean[] time;
  private ArrayList<Input> inputObject = new ArrayList<Input>();
  
  
  Input () {
    id = 0; //Consider writing a better default constructor
  }
  
  Input (int tempFrequency, int tempWaveform) {
    frequency = tempFrequency;
    waveform = tempWaveform;
  }
  
  
  Input (int tempId, int tempFrequency, int tempWaveform, int tempChannel, int tempChord, boolean[] tempTime) {
    frequency = tempFrequency;
    waveform = tempWaveform;
    channel = tempChannel;
    time = tempTime;
    id = tempId;
    chord = tempChord;
  }
  
  int getChord(){
    return chord;
  }
  ArrayList getInputObject(){
    return inputObject;
  } 
  
  
void  setTimeToFalse(int newTime) {
  this.time[newTime] = false;
  }
  
  int getID () {
    return id;
  }
  
  float getFrequency () {
    switch (frequency) {
      case 0: return 261.63; //C4
      case 1: return 293.66; //D4
      case 2: return 392.00; //G4
      case 3: return 440.00; //A4
      
      //for major 3rd
      case 4: return 329.6;
      case 5: return 370.0;
      case 6: return 493.9;
      case 7: return 554.4; //?
      
      //cases for the major 7th
      
      
      default: return 440.00; //A4
    }
    
  }
  
  Wavetable getWaveform () {
    switch (waveform) {
      case 0: return Waves.SQUARE;
      case 1: return Waves.TRIANGLE;
      case 2: return Waves.SAW;
      case 3: return Waves.QUARTERPULSE;
      default: return Waves.SQUARE;
    }
  }
  
  int getChannel () {
    return channel;
  }
  
  boolean getTime (int index) {
    return time[index];
  }
  
  boolean getFalseValues(){
    int falseCounter = 0;

     for( int j= 0; j<time.length; ++j){
       if (!time[j]){
         ++falseCounter;}
     }
     if (falseCounter == 8) return true;
      else
      return false;

  }
  
  int getTrueValues(){
    int trueCounter = 0;
    for( int j= 0; j<time.length; ++j){
       if (time[j]){
         ++trueCounter;}
     }
    return trueCounter;
  }
  
   public String toString() {
    String message = "Object " + id;
    return message;
  }

}