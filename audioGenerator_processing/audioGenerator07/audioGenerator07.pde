import processing.serial.*;
import ddf.minim.*;
import ddf.minim.ugens.*;
import java.util.ArrayList;


//Global variables
//These variables are for serial communication

//commented out temporarily, for testing purposes
boolean pressed = false; 
//boolean pressed = true;

boolean[] frameToBoolean = new boolean[16];
// this is the list of input notes
ArrayList<Input> inputObjects = new ArrayList<Input>();
Input tempInput = null;
int id;
int note;
int waveform;

Serial myPort;

ADSR[] adsr = new ADSR[12];

//These are for minim audio
AudioOutput aOut;
Minim minim;

//Input objects and arrays
Input[][] soundLoop = new Input[16][4]; // [16] = 4 seconds [4] the 4 different channels
boolean[] dummyBoolArray = new boolean[] {false, false, false, false, false, false, false, false};
Input dummy = new Input(-1, -1, -1, -1, -1, dummyBoolArray);

//Oscillators - used to avoid NullPointerException for playTemporary()
Oscil tempOsc = new Oscil(0,0,Waves.SINE);


//General variables used in the program
boolean alreadyPlayed = false;

String val="";
//Testing
//3rd channel only for noise
//String val =     "3, 1, 0, 2, 0, 0, 0, 0, 0, 1, 1, 0, ";  //note, waveform, channel, chord, rest are boolean values
//String valTest1 = "2, 0, 1, 2, 1, 1, 0, 0, 0, 0, 0, 0, ";
String valTest1 = "n, 1, 0, 0, 0, 0, 0, 0, 0, ";
//String valTest2 = "u";
String valTest2 = "2, 0, 1, 2, 0, 1, 0, 0, 0, 0, 0, 0, ";

void setup() {
  minim = new Minim(this);
  
  
  //commented out for testing purposes
  printArray(Serial.list()); //Writes the port(s) in use 
  String portName = Serial.list()[0];
  myPort = new Serial(this, portName, 9600);
  myPort.bufferUntil('\n');
  
  aOut = minim.getLineOut();
  for (int i = 0; i < adsr.length; ++i)
    adsr[i] = new ADSR(0.0, 0.0, 0.0, 0.0, 0.0);
 
  frameRate(2.5); //This equals to 150BPM

}

void draw() {
if (pressed) {
  
     splitMessage(val);
     overrideInputObjects();
     createSoundArray(inputObjects);
     print2Darray();
    
     /*splitMessage(valTest1);
     overrideInputObjects();
     createSoundArray(inputObjects);
     print2Darray();
     
    /* splitMessage(valTest2);
     overrideInputObjects();
     createSoundArray(inputObjects);
     print2Darray();*/
     
     pressed = false;
   }
   else {
     tempOsc.unpatch(aOut);
     alreadyPlayed = false;
   } 
 // playNoise();
  
  playSoundArray();
}

//for testing purposes: white noise without envelope
void playNoise(){
  Noise testNoise = new Noise(0.8f, Noise.Tint.WHITE);
  testNoise.patch(aOut);
  delay(500);
  testNoise.unpatch(aOut);
  delay(1000);
  
}
/*
//for testing purposes: white noise with envelope
void playNoise(){
  Noise testNoise = new Noise(0.8f, Noise.Tint.WHITE);
  adsr[adsr.length-1] = new ADSR(0.8, 0.04, 0.09, 0.1, 0.1);
  testNoise.patch(adsr[adsr.length-1]);
  adsr[adsr.length-1].patch(aOut);
  adsr[adsr.length-1].noteOn();
  delay(500);
  adsr[adsr.length-1].noteOff();
  delay(1000);
}
*/

//For testing purposes only
void print2Darray(){
   for (int i = 0; i < soundLoop.length; i++) {
       for (int j = 0; j < soundLoop[i].length; j++) {
         print(soundLoop[i][j] + " ");
       }
       println("");
     }
     println("");
}

void playTemporary () {
  if (!alreadyPlayed) {
    Input tempInput = new Input(note, waveform); //Use this for inpiration to adding objects to SoundLoop
    //Fill oscillator with note and wave information
    tempOsc = new Oscil(tempInput.getFrequency(), 1.0f, tempInput.getWaveform());
    //Play oscillator as long as that type of serial message is true
    tempOsc.patch(aOut);
    //Stop oscillator when that type is false
    alreadyPlayed = true;
  }
  
}

void createSoundArray (ArrayList<Input> inputs) {
  //Store values in a 2D array with x = channels, y = i (current frame)
  for (int i = 0; i < soundLoop.length; i++) {
     for (int j = 0; j < soundLoop[i].length; j++) {
       for (int k = 0; k < inputs.size(); k++) {
         if (j == inputs.get(k).getChannel() && inputs.get(k).getTime(i)) {
           soundLoop[i][j] = inputs.get(k);
         }
       }
       if (soundLoop[i][j] == null)
         soundLoop[i][j] = dummy;
     }
  }
}

//this function was made for code optimization: does not work
void playEnvelope(float frequency, Wavetable waveform, ADSR adsrTemp){
  println("entered function");
Oscil mainOsc = new Oscil(frequency, 0.8f, waveform);
adsrTemp = new ADSR(0.6, 0.04, 0.09, 0.1, 0.1);
mainOsc.patch(adsrTemp);
adsrTemp.patch(aOut);
adsrTemp.noteOn();
}

//Delay time should be fixed - 1 note has a time of 0.5 seconds
void playSoundArray () {
  for (int i = 0; i < soundLoop.length; i++) {
    for (int j = 0; j < soundLoop[i].length; j++) {
      int beforeI = i-1;
      if (beforeI < 0)  beforeI = soundLoop.length-1;
      int afterI = i+1;
      if (afterI == soundLoop.length) afterI = 0;   
    
    //start playing the sound
     if (soundLoop[i][j].getID() != -1 && (soundLoop[i][j].getID() != soundLoop[beforeI][j].getID())) {
       //if in noise channel
       if(soundLoop[i][j].getChannel() == 3){
         Noise testNoise = new Noise(0.8f, Noise.Tint.WHITE);
         adsr[j] = new ADSR(0.8, 0.04, 0.09, 0.1, 0.1);
         testNoise.patch(adsr[j]);
       }
       //if in other channel
       else{
         Oscil mainOsc = new Oscil(soundLoop[i][j].getFrequency(), 0.8f, soundLoop[i][j].getWaveform());
         adsr[j] = new ADSR(0.6, 0.04, 0.09, 0.1, 0.1);
         mainOsc.patch(adsr[j]);
       }
           adsr[j].patch(aOut);
           adsr[j].noteOn();
           
           //for dyad
           if (soundLoop[i][j].getChord() != 0){
             int newJ = j+4;
             adsr[newJ] = new ADSR(0.6, 0.04, 0.09, 0.1, 0.1);
             Oscil secondOsc = new Oscil(soundLoop[i][j].getDyad(), 0.8f, soundLoop[i][j].getWaveform());
             secondOsc.patch(adsr[newJ]);
             adsr[newJ].patch(aOut);
             adsr[newJ].noteOn();
           
             //for triad
             if (soundLoop[i][j].getChord() == 2){
                newJ = j+8;
                adsr[newJ] = new ADSR(0.6, 0.04, 0.09, 0.1, 0.1);
                Oscil thirdOsc = new Oscil(soundLoop[i][j].getTriad(), 0.8f, soundLoop[i][j].getWaveform());
                thirdOsc.patch(adsr[newJ]);
                adsr[newJ].patch(aOut);
                adsr[newJ].noteOn();
             }
           } //<>// //<>//
           println("Playing " + soundLoop[i][j].getID());
     }
  
  //start the release of the sound envelope
      else if (soundLoop[i][j].getID() != -1 && (soundLoop[i][j].getID() != soundLoop[afterI][j].getID())) {
         adsr[j].noteOff();
         if (soundLoop[i][j].getChord() != 0){
           int newJ = j+4;
           adsr[newJ].noteOff();
           if (soundLoop[i][j].getChord()== 2){
                newJ = j+8;
                adsr[newJ].noteOff();
           }
         }
          println("Releasing " + soundLoop[i][j].getID());
       }
     }
     delay(250);
  }
}

//put boolean inpur from arduino into boolean array of an Input object
void makeFrames(int value, String[] parts){
  int j = 0;
  for (int i = value; i < parts.length; i++){
    int k = j+1;
    if (parts[i].trim().indexOf('1') >= 0){
      frameToBoolean[j] = true;
      frameToBoolean[k] = true;
    }
    j = j+2;
  }
}

void splitMessage(String message) {
  String[] parts = split(message,','); 
  frameToBoolean = new boolean[16];
 
  //if the "Undo" button is pressed
  if (parts[0].trim().indexOf('u') >= 0){
    if (inputObjects.size() > 0){
      tempInput.setID(-1); //<>//
      inputObjects.remove(inputObjects.get(inputObjects.size()-1));
    }
    return;
  }
  
  //if the noise button is pressed
  if (parts[0].trim().indexOf('n') >= 0){
     makeFrames(1, parts);
     tempInput = new Input(id, 3, 0, frameToBoolean);
  }
  else{
  note = Integer.parseInt(parts[0].trim()); 
  waveform = Integer.parseInt(parts[1].trim());
  int channel = Integer.parseInt(parts[2].trim());
  int chord = Integer.parseInt(parts[3].trim());
  makeFrames(4, parts);
  tempInput = new Input(id, note, waveform, channel, chord, frameToBoolean);
  }
    ++id;
 }
  

 void overrideInputObjects(){
   for( int i= 0; i< inputObjects.size(); ++i){
      if (tempInput.getChannel() == inputObjects.get(i).getChannel()){ 
        for( int j= 0; j<frameToBoolean.length; ++j){
         if (inputObjects.get(i).getTime(j) && tempInput.getTime(j)){
            inputObjects.get(i).setTimeToFalse(j); 
        }   
      }
   }
   //make function remove those objects, that have only false values in them
   if (inputObjects.get(i).getFalseValues()){
          inputObjects.remove(inputObjects.get(i));
      }
   }
   if (tempInput.getID() != -1)
     inputObjects.add(tempInput);  
     
 }
 

//commented out for testing purposes

void serialEvent(Serial p) { 
  //Detects if serial communication is actives
  p = myPort;
  val = p.readStringUntil('\n');
  pressed = true;
}