﻿using UnityEngine;
using System.Collections;
using System.IO.Ports;
using System;

public class GameManager : MonoBehaviour {

	SerialPort stream = new SerialPort("COM4", 9600);
	public string incMessage;
	public bool spawning = false;
	int note;
	int waveform;
	ParticleSystemRenderer psr;
	GameObject spawnedPlanet;
	GameObject spawnedWaveformer;

	public GameObject NoteCPlanet;
	public GameObject NoteDPlanet;
	public GameObject NoteGPlanet;
	public GameObject NoteAPlanet;

	public GameObject squareWave;
	public GameObject triangleWave;

	public Transform spawnPoint;

	// Use this for initialization
	void Start () {
		stream.ReadTimeout = 50;
		stream.Open ();
	}

	// Update is called once per frame
	void Update () {
		StartCoroutine (
			AsynchronousReadFromArduino (
				(string s) => Debug.Log (s),
				() => Debug.LogError ("Error!"),
				0.5f
			)
		);
	}

	public IEnumerator AsynchronousReadFromArduino(Action<string> callback, Action fail = null, float timeout = float.PositiveInfinity) {
		DateTime initialTime = DateTime.Now;
		DateTime nowTime;
		TimeSpan diff = default(TimeSpan);

		string dataString = null;

		do {
			try {
				dataString = stream.ReadLine ();
				//dataString = incMessage;
			} catch (TimeoutException) {
				dataString = null;
			}

			if (dataString != null) {
				incMessage = dataString;
				callback (dataString);
				SplitMessage();
				Debug.Log("Note: " + note + " , Waveform: " + waveform);
				SpawnPlanet();
				spawning = true;
				yield return null;
			} else
				spawning = false;
				yield return new WaitForSeconds (0.05f);

			nowTime = DateTime.Now;
			diff = nowTime - initialTime;
		} while (diff.Milliseconds < timeout);

		if (fail != null)
			fail ();
		yield return null;
	}

	public void SplitMessage () {
		string[] messageParts = incMessage.Split (new string[] {","}, StringSplitOptions.None);
		note = int.Parse (messageParts [0]);
		waveform = int.Parse(messageParts[1]);
	}

	public void SpawnPlanet () {
		switch (note) {
		case 0: 
			spawnedPlanet = Instantiate (NoteCPlanet, spawnPoint.position, spawnPoint.rotation) as GameObject;
			break;
		case 1:
			spawnedPlanet = Instantiate (NoteDPlanet, spawnPoint.position, spawnPoint.rotation) as GameObject;
			break;
		case 2: 
			spawnedPlanet = Instantiate (NoteGPlanet, spawnPoint.position, spawnPoint.rotation) as GameObject;
			break;
		case 3:
			spawnedPlanet = Instantiate (NoteAPlanet, spawnPoint.position, spawnPoint.rotation) as GameObject;
			break;
		}

		switch (waveform) {
		case 0: 
			spawnedWaveformer = Instantiate (squareWave, new Vector3 (-4.1f, 0.0f, 0.0f), Quaternion.Euler (0, -90, 0)) as GameObject;
			spawnedWaveformer.transform.SetParent (spawnedPlanet.transform, false);
			break;
		case 1:
			spawnedWaveformer = Instantiate (triangleWave, new Vector3(-4.1f,0.0f,0.0f), Quaternion.Euler(0,-90,0)) as GameObject;
			spawnedWaveformer.transform.SetParent (spawnedPlanet.transform, false);
			break;
		}
	}
}
