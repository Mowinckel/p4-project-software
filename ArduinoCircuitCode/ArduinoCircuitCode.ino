//
//first arduino in blueprint
//
//leds for waveform
const int numberLeds = 4;
int led[numberLeds] = {8, 9, 10, 11};
int ledState[numberLeds];

//buttons for notes
const int numberNotes = 5;
int notes[numberNotes] = {3,4,5,6,7};
int buttonState[numberNotes];

//pin for potentiometer
int potPin = A0;
//potentiometers value
int val = 0; 

//led and button for beat
int ledbeat = 15;
int buttonbeat = 14;

//stuff used for the beat
int timer = 0;
int beat;
int state = LOW;
int previous = HIGH;

//
//second arduino in blueprint
//
//buttons and led for triadic
int led1 = 19;
int led2 = 21;
int button1 = 20;
int button2 = 22;
//buttons for navigation
int buttonUp = 16;
int buttonDown = 17;

//button and led for undo
int buttonUndo = 18;
int ledUndo = 23;

//state1, state2, third, seventh, previous1, previous2 is for triadic
//state3, undo, previous3 is for undo
int state1 = LOW;
int state2 = LOW;
int state3 = LOW;
int third;
int seventh;
int undo;
int previous1 = HIGH;
int previous2 = HIGH;
int previous3 = HIGH;

void setup() 
{
  //first arduino in blueprint
  pinMode(led[0],OUTPUT);
  pinMode(led[1],OUTPUT);
  pinMode(led[2],OUTPUT);
  pinMode(led[3],OUTPUT);
  pinMode(buttonbeat,INPUT);
  pinMode(ledbeat,OUTPUT);
  
  pinMode(notes[0], INPUT);
  pinMode(notes[1], INPUT);
  pinMode(notes[2], INPUT);
  pinMode(notes[3], INPUT);
  pinMode(notes[4], INPUT);

  //second arduino in blueprint
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(button1, INPUT);
  pinMode(button2, INPUT);
  pinMode(buttonUp, INPUT);
  pinMode(buttonDown, INPUT);
  pinMode(buttonUndo, INPUT);
  pinMode(ledUndo, INPUT);
  
  Serial.begin(9600);
}
 
void loop() 
{
  //code for buttons function
  waveformButton();
  triadicButton();
  beatButton();
  undoButton();

  //code for sending to port
  sendTriadic();
  sendNavigation();
  sendUndo();
  sendNote();
  
  //Serial.println(val);
}

void waveformButton() {
    val = analogRead(potPin);
  //val = map(val, 0, 1023, 0, 4);
  //Serial.print(val);

  if (val <= 256){
    digitalWrite(led[0], HIGH);
    digitalWrite(led[1], LOW);
    digitalWrite(led[2], LOW);
    digitalWrite(led[3], LOW);
  }
  else if (val > 256 && val <= 512){
    digitalWrite(led[0], LOW);
    digitalWrite(led[1], HIGH);
    digitalWrite(led[2], LOW);
    digitalWrite(led[3], LOW);
  }
  else if (val > 512 && val <= 768){
    digitalWrite(led[0], LOW);
    digitalWrite(led[1], LOW);
    digitalWrite(led[2], HIGH);
    digitalWrite(led[3], LOW);
  }
  else {
    digitalWrite(led[0], LOW);
    digitalWrite(led[1], LOW);
    digitalWrite(led[2], LOW);
    digitalWrite(led[3], HIGH);
  }
  delay(15);
}

void triadicButton() {

  third = digitalRead(button1);
  seventh = digitalRead(button2);
  
  if (third == HIGH && previous1 == LOW){
    if (state1 == HIGH)
      state1 = LOW;
    else
      state1 = HIGH;
    if (state1 == HIGH)
      state2 = LOW;
  }
  digitalWrite(led1, state1);
  previous1 = third;
  
  if (seventh == HIGH && previous2 == LOW){
    if (state2 == HIGH)
      state2 = LOW;
    else
      state2 = HIGH;
    if (state2 == HIGH)
      state1 = LOW;
  }
  digitalWrite(led2, state2);
  previous2 = seventh;
  
}

void beatButton() {
  beat = digitalRead(buttonbeat);
  
  if (beat == HIGH && previous == LOW){
   if (state == HIGH)
     state = LOW;
   else
     state = HIGH; 
  }
  digitalWrite(ledbeat, HIGH);
  previous = beat;
}

void undoButton() {
    undo = digitalRead(buttonUndo);
  
  if (undo == HIGH && previous3 == LOW){
    if (state3 == HIGH)
      state3 = LOW;
    else
      state3 = HIGH;
  }
  
  digitalWrite(ledUndo, state3);
  
  previous3 = undo;
}

void sendNote() {
  
  for(int y=0;y<numberLeds;y++){
  
    for(int x=0;x<numberNotes;x++){
      
      int buttonState = digitalRead(notes[x]);
      int ledState = digitalRead(led[y]);
      
      if (buttonState == HIGH && ledState == HIGH){
        //Serial.print(notes[x]);
        String message = String(buttonState+x)+","+String(y);
        //Serial.print(buttonState+x);
        //Serial.print(",");
        //Serial.println(y);
        Serial.println(message);
        delay(100);
      }
    }
  }
}

void sendTriadic(){
  
  if (state1 == HIGH && state2 == LOW){
    Serial.println("3rd");
  }
  else if (state1 == LOW && state2 ==HIGH){
    Serial.println("7th");
  }
  //delay(500);
  
}

void sendNavigation(){
  
  int buttonUpState = digitalRead(buttonUp);
  int buttonDownState = digitalRead(buttonDown);
  
  if (buttonUpState == LOW){
    Serial.println("up");
    delay(20);
  }
  
  if (buttonDownState == LOW){
    Serial.println("down");
    delay(20);
  }
}

//this one doesn't work yet
void sendUndo(){
  
  int buttonUndoState = digitalRead(buttonUndo);
  int ledUndoState = digitalRead(ledUndo);
  
  if (buttonUndoState == LOW && ledUndoState == HIGH){
    Serial.println("undo");
    delay(20);
  }
  
}

//sendBeat still not made yet. Should work just like the undo button
//void sendBeat() {
//
//}


